<?php
	session_start();
	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ville.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/formation.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ami.inc.php';

	// Récuperation de l'étudiant.
	$e = Eleve::selectById( $dbh, $_GET['id'] );
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
		<!-- JQuery -->
		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
		<!-- Script ami -->
		<script
			type="text/javascript"
			src="./js/ami.js"></script>
		<!-- Script offre -->
		<script
			type="text/javascript"
			src="./js/offre.js"></script>

		<script>
			// Fonction de récuperation des avis aux offres.
			function queryAvis() {
				var q = $( '#search-avis' ).val();
				var search = 1;

				// Requête AJAX
				$.ajax({
					url : './offre-query.php',
					method : 'POST',
					data : {
						search : search,
						q : q,
						id_utilisateur : null,
						id_eleve : <?php echo $e->id ?>,
						id_entreprise : null,
					},
					success : function( data ) {
						$( '#results-avis' ).html( data );
					},
					dataType : 'text'
				});
			}

			// Fonction de récuperation des offres publiées.
			function queryPubs() {
				var q = $( '#search-pubs' ).val();
				var search = 1;

				// Requête AJAX
				$.ajax({
					url : './offre-query.php',
					method : 'POST',
					data : {
						search : search,
						q : q,
						id_utilisateur : <?php echo $e->id ?>,
						id_eleve : null,
						id_entreprise : null,
					},
					success : function( data ) {
						$( '#results-pubs' ).html( data );
					},
					dataType : 'text'
				});
			}

			// Fonction d'affichage des onglets.
			function openTab( name ) {
				var i;
				var tabs = document.getElementsByClassName( 'tab' );

				for( i = 0; i < tabs.length; i++ ) {
					tabs[i].style.display = "none";
				}

				document
					.getElementById( name )
					.style
					.display
						= "block";
			}

			// Fonction d'affichage du modal de suppression d'une offre.
			function showOffreSuppr( id ) {
				$( '#offre-suppr-modal' ).load(
					'./inc/offre-suppr-modal.inc.php?id=' + id
				);

				$( '#offre-suppr-modal' ).show();
			}

			// Quand le bouton 'Oui' du modal de suppression d'une offre est cliqué.
			function offreSupprOui( id ) {
				// Suppression de l'offre.
				queryOffre( id, 'Supprimer' );

				$( '#offre-suppr-modal' ).hide();

				// Màj après suppression.
				$( '#search-pubs' ).keyup();
			}

			// Quand la page est ready :
			$( document ).ready( function() {
				// Quand du texte est rentré dans la textbox #search-avis :
				$( '#search-avis' ).keyup( function() {
					queryAvis();
				});

				// Quand du texte est rentré dans la textbox #search-pubs :
				$( '#search-pubs' ).keyup( function() {
					queryPubs();
				});

				// Récupèration de tous les avis.
				queryAvis();
				// Récupèration de toutes les publications.
				queryPubs();
			});
		</script>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once( './inc/header.inc.php' ); ?>

		<!-- Main -->
		<main class="w3-theme-l4">

			<!-- Layout -->
			<div class="w3-container w3-row">

				<!-- Sidebar -->
				<?php require_once( './inc/sidebar-etudiant.inc.php' ); ?>

				<!-- Colonne principale -->
				<div class="w3-rest w3-mobile">

					<!-- Description -->
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<h1><?php echo $e->prenom, ' ', $e->nom ?></h1>
						<p><?php echo nl2br( $e->preferences ) ?></p>
					</div>

					<div class="w3-card w3-round w3-margin w3-white">

						<!-- Onglets -->
						<div class="w3-bar w3-light-gray w3-round">

							<!-- Onglet avis -->
							<button
								class="w3-bar-item w3-button"
								onclick="openTab( 'tab-avis' )">
								Avis
							</button>

							<!-- Onglet offres publiées -->
							<button
								class="w3-bar-item w3-button"
								onclick="openTab( 'tab-pubs' )">
								Offres publiées
							</button>

							<!-- Onglet amis -->
							<button
								class="w3-bar-item w3-button"
								onclick="openTab( 'tab-amis' )">
								Amis
							</button>
						</div>

						<div class="w3-container w3-border-top">
							<!-- Avis -->
							<div id="tab-avis" class="tab">
								<div class="w3-cell-row">
									<div class="w3-cell w3-cell-middle w3-mobile">
										<h1>Avis</h1>
									</div>

									<!-- Recherche -->
									<div class="w3-cell w3-cell-middle w3-mobile">
										<input
											type="text"
											placeholder="Recherche"
											id="search-avis"
											class="w3-right w3-border w3-white w3-margin-top w3-margin-bottom w3-mobile"/>
									</div>
								</div>
								<div id="results-avis">
									<p>Chargement ...</p>
								</div>
							</div>

							<!-- Offres publiées -->
							<div id="tab-pubs" class="tab" style="display: none">

								<div class="w3-cell-row">
									<div class="w3-cell w3-cell-middle w3-mobile">
										<h1>Offres publiées</h1>
									</div>

									<!-- Recherche -->
									<div class="w3-cell w3-cell-middle w3-mobile">
										<input
											type="text"
											placeholder="Recherche"
											id="search-pubs"
											class="w3-right w3-border w3-white w3-margin-top w3-margin-bottom w3-mobile"/>
									</div>
								</div>
								<div id="results-pubs">
									<p>Chargement ...</p>
								</div>
							</div>

							<!-- Amis -->
							<div id="tab-amis" class="tab" style="display: none">

								<div class="w3-cell-row">
									<div class="w3-cell w3-cell-middle w3-mobile">
										<h1>Amis</h1>
									</div>

									<!-- Recherche -->
									<div class="w3-cell w3-cell-middle w3-mobile">
										<input
											type="text"
											placeholder="Recherche"
											id="search-amis"
											class="w3-right w3-border w3-white w3-margin-top w3-margin-bottom w3-mobile"/>
									</div>
								</div>
								<div id="results-amis">
									<p>Chargement ...</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<!-- Modal de suppression d'une offre. -->
			<div id="offre-suppr-modal" class="w3-modal"></div>
		</main>

		<!-- Pied -->
		<?php require_once( './inc/footer.inc.php' ); ?>
	</body>
</html>
