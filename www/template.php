<?php
	session_start();
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once( './inc/header.inc.php' ); ?>

		<!-- Main -->
		<main class="w3-theme-l4">

			<!-- Layout -->
			<div class="w3-container w3-row">

				<!-- Sidebar -->
				<?php require_once( './inc/sidebar.inc.php' ); ?>

				<!-- Colonne principale -->
				<div class="w3-rest w3-mobile">
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<h4>Lorem ipsum dolor sit amet.</h4>
						<hr/>
						<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<h4>Lorem ipsum dolor sit amet.</h4>
						<hr/>
						<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
				</div>
			</div>

			<div class="w3-container">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once( './inc/footer.inc.php' ); ?>
	</body>
</html>
