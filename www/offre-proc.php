<?php
session_start();
require_once dirname(__FILE__) . '/inc/bdd.inc.php';
require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
require_once dirname(__FILE__) . '/inc/classes/offre.inc.php';

if( isset( $_SESSION['utilisateur'] ) ) {
	$u = unserialize( $_SESSION['utilisateur'] );
} else {
	header( 'Location: ./' );
	exit();
}

try {
	switch( $_POST['submit'] ) {

		// --- Ajout ---
		case 'Ajouter':

			// Compilation des attributs.
			$attrs = array(
				"titre"          => $_POST['titre'         ],
				"description"    => $_POST['description'   ],
				"debut"          => $_POST['debut'         ],

				"fin"
					=> isset( $_POST['fin'] )
					?  $_POST['fin']
					:  NULL,

				"salaire_hr_ht"
					=> isset( $_POST['salaire_hr_ht' ] )
					?  $_POST['salaire_hr_ht']
					:  NULL,

				"publication"    => date( 'Y-m-d' ),
				"note"           => NULL,
				"commentaire"    => NULL,
				"report"         => 0,
				"id_Utilisateur" => $u->id,
				"id_Entreprise"  => $_POST['id_entreprise' ],
				"id_Eleve"       => NULL
			);

			$no = New Offre( $attrs );
			Offre::insert( $dbh, $no );

			header(
				'Location: ./entreprise-view.php?id='
				. $no->id_entreprise
			);

			break; // 'Ajouter'

		// --- Modification ---
		case 'Modifier':

			/* NOTE: (EDITS DE LA NUIT) $_GET['id'] n'est pas passé sur la page de proc. Il faudrait faire passer l'id de l'offre via un input hidden :*/

			if( !isset( $_POST['id'] ) ) break;

			// Récuperation de l'offre.
			$o = Offre::selectById( $dbh, $_POST['id'] );

			/* NOTE: (EDITS DE LA NUIT) Aussi actuellement rien ne nous empêche de modifier n'importe-quelle offre en passant les inputs de hidden à number.

			C'est ici qu'il faudra faire toutes les vérifs, en comparant l'id de l'étudiant / entreprise connécté avec l'id d'utilisateur / entreprise concernée de l'offre. */

			$isAllowed
				= ( $u->id == $o->id_utilisateur )
				| ( $u->id == $o->id_entreprise  );

			if( !$isAllowed ) die( 'Casse toi.' );

			/* NOTE: (EDITS DE LA NUIT) C'est cool mais si tu réfléchis bien, ça pose encore des soucis. On pourait notament écraser les reports, avis etc d'une offre à laquelle on à déja accès avec la méthode décrite plus haut, voire carrément hijacker une offre, mais :dumb: */

			// Màj de l'offre.

			$o->titre       = $_POST['titre'      ];
			$o->description = $_POST['description'];
			$o->debut       = $_POST['debut'      ];

			$o->fin
				= isset( $_POST['fin'] )
				?  $_POST['fin']
				:  NULL;

			$o->salaire_hr_ht
				= isset( $_POST['salaire_hr_ht' ] )
				?  $_POST['salaire_hr_ht']
				:  NULL;

			Offre::update( $dbh, $_POST['id'], $o );

			header(
				'Location: ./entreprise-view.php?id='
				. $o->id_entreprise
			);

			break; // 'Modifier'

			// --- Supprimer ---
			case 'Supprimer':
				Offre::delete( $dbh, $_POST['id'] );
				break; // 'Supprimer'
	}
}

// Si quelque chose c'est mal passé :
catch( Exception $e ) {
	//throw $e;
	echo $e->getMessage();
}

?>
