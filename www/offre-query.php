<?php
	session_start();

	// On vérifie q'un utilisateur est connécté.
	if( !isset( $_SESSION['utilisateur'] ) )
		exit();

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/offre.inc.php';

	// L'utilisateur connécté.
	$u = unserialize( $_SESSION['utilisateur'] );

	if( isset( $_POST['search'] ) && $_POST['search'] ) {
		// Recherche initiée.
		$offres = Offre::like(
			$dbh,
			isset( $_POST['id_utilisateur'] ) ? $_POST['id_utilisateur'] : null,
			isset( $_POST['id_entreprise' ] ) ? $_POST['id_entreprise' ] : null,
			isset( $_POST['id_eleve'      ] ) ? $_POST['id_eleve'      ] : null,
			$_POST['q']
		);
	} else {
		// Pas de recherche : on récupère tout !
		$offres = Offre::selectAll( $dbh );
	}
?>

<ul id="offres" class="w3-ul w3-margin-top w3-margin-bottom multipage">

	<?php
		// Si la recherche à donnée quelque-chose :
		if( count( $offres ) != 0 ) :
		// Parcours des offres.
		foreach( $offres as $o ) :
	?>

	<li class="w3-bar page-element">
		<span class="w3-xlarge">
			<?php echo $o->titre; ?>
		</span><br/>





		<?php // NOTE: A relire et réviser. ?>

		<?php
		// Si c'est l'entreprise qui a posté cette offre.
		if( $o->id_utilisateur == $o->id_entreprise ) { ?>
			<span class="w3-text-theme w3-tag w3-theme-l4 w3-border w3-border-theme w3-mobile">
				<i class="fa fa-info"></i>&nbsp;
				<em>Géré par l'entreprise</em>
			</span>
		<?php //Sinon, afficher l'élève qui l'a posté.
		}else{
			// L'élève.
			$el = Eleve::selectById( $dbh, $o->id_utilisateur ); ?>
			<a
				href='./etudiant-view.php?id=<?php echo $el->id ?>'
				class="w3-text-gray w3-tag w3-light-gray w3-border w3-mobile"
				style="text-decoration: none;">
				<i class="fa fa-info"></i>&nbsp;
				<em>Posté par <?php echo $el->prenom." ".$el->nom; ?></em>
			</a>
		<?php } ?>





		<?php
			// L'entreprise.
			$e = Entreprise::selectById( $dbh, $o->id_entreprise );
		?>

		<!-- Entreprise -->
		<span class="w3-text-gray w3-tag w3-light-gray w3-border w3-mobile">
			<i class="fa fa-black-tie"></i>&nbsp;
			<em>
				<a
					href='./entreprise-view.php?id=<?php echo $e->id ?>'
					style='text-decoration: none;' >
					<?php echo $e->nom_entreprise ?>
				</a>
			</em>
		</span>

		<!-- Date publication -->
		<span class="w3-text-gray w3-tag w3-light-gray w3-border w3-mobile">
			<i class="fa fa-calendar"></i>
			<em><?php echo date_format( date_create( $o->publication ), "d/m/Y" ) ?></em>
		</span>

		<p><?php echo nl2br( $o->description ) ?></p>

		<?php if( $u instanceof Eleve ) : ?>

		<!-- Boutton Postuler -->
		<a
			onclick=""
			class="w3-button w3-text-theme w3-border w3-margin-bottom w3-mobile">
			<i class="fa fa-paper-plane"></i>
			Postuler
		</a>

	<?php endif; // $u instanceof Eleve ?>

		<?php
			// Drapeau d'autorisation de modification de l'offre.
			$isAllowed
				= ( $o->id_entreprise  == $u->id )  // L'entreprise concernée.
				| ( $o->id_utilisateur == $u->id ); // L'auteur de l'annonce.

			if( $isAllowed ) :
		?>

		<!-- Boutton Modifier l'offre -->
		<a
			href="./offre-view.php?id=<?php echo $o->id ?>"
			class="w3-button w3-border w3-margin-bottom w3-mobile">
			<span class="label w3-text-theme">
				<i class="fa fa-pencil"></i>
				Modifier
			</span>
		</a>

		<!-- Boutton Supprimer l'offre -->
		<a
			onclick="showOffreSuppr( <?php echo $o->id ?> )"
			class="w3-button w3-text-gray w3-border w3-margin-bottom w3-mobile">
			<i class="fa fa-minus"></i>
			Supprimer
		</a>

		<?php endif; ?>

	</li>
	<?php endforeach; // $o ?>
</ul>

<?php
	// Aucune offre n'a été trouvée.
	else :
?>

<p class="w3-text-gray"><em>Aucune offre correspondante n'a été trouvée.</em></p>

<?php endif; // count( $offre ) ?>
