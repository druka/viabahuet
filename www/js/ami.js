// Fonction d'ajout / suppression d'ami.
function queryAmi( id, action ) {
	$.ajax({
		url : './ami-proc.php',
		method : 'POST',
		data : {
			id : id,
			action: action,
		},
		success : function() {
			switch( action ) {
				case 'Ajouter':
					// Le bouton.
					var button = $( '#ami-ajout-' + id );
					button.attr( 'id', 'ami-suppr-' + id );
					button.attr( 'onclick', "queryAmi( " + id + ", 'Supprimer' )" );

					// Le label.
					var label = button.children( '.label' );
					label.attr( 'class', "label w3-text-gray" )
					label.html( "<i class=\"fa fa-minus\"></i> Retirer des amis" );
					break;

				case 'Supprimer':
					// Le bouton.
					var button = $( '#ami-suppr-' + id );
					button.attr( 'id', 'ami-ajout-' + id );
					button.attr( 'onclick', "queryAmi( " + id + ", 'Ajouter' )" );

					// Le label.
					var label = button.children( '.label' );
					label.attr( 'class', "label w3-text-theme" );
					label.html( "<i class=\"fa fa-plus\"></i> Ajouter en ami" );
					break;
			}
		},

		dataType : 'text'
	});
}
