<?php
	session_start();

	// On vérifie q'un utilisateur est connécté.
	if( !isset( $_SESSION['utilisateur'] ) )
		exit();

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ville.inc.php';

	if( isset( $_POST['search'] ) && $_POST['search'] ) {
		// Recherche initiée.
		$entreprises = Entreprise::like( $dbh, $_POST['q'] );
	} else {
		// Pas de recherche : on récupère tout !
		$entreprises = Entreprise::selectAll( $dbh );
	}

	// L'utilisateur connécté.
	$u = unserialize( $_SESSION['utilisateur'] );
?>

<ul id="entreprises" class="w3-ul w3-margin-top w3-margin-bottom">

	<?php
		// Si la recherche à donnée quelque-chose :
		if( count( $entreprises ) != 0 ) :
		// Parcours des élèves;
		foreach( $entreprises as $e ) :
	?>

	<li class="w3-bar">

		<!-- Image de profile -->
		<div class="w3-container w3-cell w3-center w3-mobile">
			<div
				class="vb-profilepic vb-tiny w3-border"
				style="background-image: url( './images/<?php echo $e->photo ?>' )">
			</div><br/>
			<?php if( $u->id == $e->id ) : ?>
			<span class="w3-tag w3-theme">Vous</span>
			<?php endif // $u->id == $e->id ?>
		</div>

		<!-- Informations -->
		<div class="w3-container w3-cell w3-mobile">
			<!-- Nom de l'entreprise -->
			<span class="w3-xlarge"><?php echo $e->nom_entreprise ?></span><br/>

			<?php
				// La ville ou est situé l'entreprise.
				$v = Ville::selectById( $dbh, $e->id_ville );
			?>

			<?php
				// Si l'entreprise à un site web, on l'affiche.
				if( $e->site_web ) :
			?>

			<!-- Site web -->
			<span class="w3-margin-right w3-text-gray w3-mobile">
				<i class="fa fa-link"></i>
				<em><a href="//<?php echo $e->site_web ?>"><?php echo $e->site_web ?><a></em>
			</span>

			<?php endif; // $e->site_web  ?>

			<!-- Localisation -->
			<span class="w3-margin-right w3-text-gray w3-mobile">
				<i class="fa fa-map-marker"></i>
				<em>
					<?php
						echo
							$e->no_voie, ' ',
							$e->voie, ' - ',
							$v->cp, ' ',
							$v->nom
						?>
				</em>
			</span>

			<!-- Description -->
			<p><?php echo nl2br( $e->desc_entreprise ); ?></p>

			<!-- Boutton Consulter -->
			<a
				href="./entreprise-view.php?id=<?php echo $e->id ?>"
				class="w3-button w3-text-theme w3-border w3-margin-bottom w3-mobile">
				<i class="fa fa-eye"></i>
				Consulter
			</a>

			<!-- Boutton Ajouter une offre -->
			<a
				href="./offre-view.php?id_entreprise=<?php echo $e->id ?>"
				class="w3-button w3-text-theme w3-border w3-margin-bottom w3-mobile">
				<i class="fa fa-plus"></i>
				Ajouter une offre
			</a>

		</div>
	</li>

	<?php
		endforeach; // $e
	?>

</ul>

<?php
	// Aucune entreprise n'a été trouvée.
	else :
?>

<p class="w3-text-gray"><em>Aucune entreprise correspondante n'a été trouvée.</em></p>

<?php endif; // count( $entreprises ) ?>
