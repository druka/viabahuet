<?php
session_start();
require_once dirname(__FILE__) . '/inc/bdd.inc.php';
require_once dirname(__FILE__) . '/inc/classes/utilisateur.inc.php';
require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
require_once dirname(__FILE__) . '/inc/classes/ami.inc.php';

if( isset( $_SESSION['utilisateur'] ) )
	$u = unserialize( $_SESSION['utilisateur'] );

try {
	switch( $_POST['action'] ) {

		// --- Ajout ---
		case 'Ajouter':

			$na = New Ami(
				array(
					'id_Utilisateur_1' => $u->id,
					'id_Utilisateur_2' => $_POST['id'],
					'accepter'         => 0
				)
			);

			// Insertion
			Ami::insert($dbh, $na);

			break; // 'Ajouter'

		// --- Supprimer ---
		case 'Supprimer':
			Ami::delete( $dbh, $u->id, $_POST['id'] );
			break; // 'Supprimer'

	}
}

// Si quelque chose c'est mal passé :
catch( Exception $e ) {
	//throw $e;
	echo $e->getMessage();
}

?>
