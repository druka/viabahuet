<header class="w3-top w3-theme-d5">
	<div
		id="navi"
		class="w3-container">
		<a
			class="w3-bar-item w3-button w3-hide-small w3-theme-d3 w3-hover-white"
			href="./">
			Via<strong>Bahuet</strong>
		</a>
		<a
			class="w3-bar-item w3-button w3-hover-white"
			href="./offre.php">
			<i class="fa fa-briefcase"></i>
			<span class="w3-hide-small">Offres</span>
		</a>
		<a
			class="w3-bar-item w3-button w3-hover-white"
			href="./entreprise.php">
			<i class="fa fa-black-tie"></i>
			<span class="w3-hide-small">Entreprises</span>
		</a>
		<a
			class="w3-bar-item w3-button w3-hover-white"
			href="./etudiant.php">
			<i class="fa fa-graduation-cap"></i>
			<span class="w3-hide-small">Etudiants</span>
		</a>

		<?php if( isset( $_SESSION['utilisateur'] ) ) : ?>
		<?php $u = unserialize( $_SESSION['utilisateur'] ); ?>

		<!-- Menu utilisateur. -->
		<div class="w3-dropdown-hover w3-right">
			<div class="w3-button w3-theme-d3">
				<i class="fa fa-user"></i>
				<span class="w3-hide-small">
					<?php echo $u->prenom, ' ', $u->nom ?>
				</span>
			</div>

			<div
				class="w3-dropdown-content w3-bar-block w3-card-4"
				style="width: 18em; right: 0;">
					<a class="w3-bar-item w3-button" href="#">Link 1</a>
					<a class="w3-bar-item w3-button" href="#">Link 2</a>
					<a class="w3-bar-item w3-button" href="./profile.php">Mon profile</a>
					<a class="w3-bar-item w3-button" href="./deconnexion.php">Déconnexion</a>
			</div>
		</div>

		<?php endif; // isset( $_SESSION['utilisateur'] ) ?>

	</div>
</header>
