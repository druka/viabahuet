<?php

require_once dirname( __FILE__ ) . '/bdd.inc.php';

switch ( $_GET['u'] ) {
	case 'etudiant':
		require_once dirname( __FILE__ ) . '/classes/eleve.inc.php';
		require_once dirname( __FILE__ ) . '/classes/formation.inc.php';
		$u = New Eleve();
		break;

	case 'entreprise':
		require_once dirname( __FILE__ ) . '/classes/entreprise.inc.php';
		$u = New Entreprise();
		break;

	default:
		exit();
		break;
}

require_once dirname( __FILE__ ) . '/profile-form.inc.php';

?>
