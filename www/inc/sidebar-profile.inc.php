<?php
	require_once dirname(__FILE__) . '/classes/ville.inc.php';
	require_once dirname(__FILE__) . '/classes/formation.inc.php';
?>

<div id="sidebar" class="w3-col w3-mobile">
	<div class="w3-container w3-card w3-round w3-margin w3-white">

		<!-- L'image de profile. -->
		<div class="w3-center w3-margin-top w3-margin-bottom">
			<div
				class="vb-profilepic vb-large w3-border"
				style="background-image: url( './images/<?php echo $u->photo ?>' )">
			</div>
		</div>

		<p class="w3-center">
			<?php if( $u instanceof Eleve ) : ?>

			<!-- Le nom et prénom. -->
			<strong><?php echo $u->prenom, ' ', $u->nom  ?></strong>

			<?php else : ?>

			<!-- Le nom de l'entreprise. -->
			<strong><?php echo $u->nom_entreprise ?></strong>

			<?php endif; // $u instanceof Eleve ?>
		</p>

		<hr/>

		<?php if( $u instanceof Eleve ) : ?>

		<?php
			// La formation de l'étudiant.
			$f = Formation::selectById( $dbh, $u->id_formation );
		?>

		<!-- Formation -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-graduation-cap fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $f->nom ?>
			</span>
		</p>

		<?php endif; // $u instanceof Eleve ?>

		<?php if( $u instanceof Entreprise ) : ?>

		<!-- Site web -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-link fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<a href="\\<?php echo $u->site_web ?>">
					<?php echo $u->site_web ?>
				</a>
			</span>
		</p>

		<?php endif; // $u instanceof Entreprise ?>

		<?php
			// La ville.
			$v = Ville::selectById( $dbh, $u->id_ville );
		?>

		<!-- Ville -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $v->cp, ' ', $v->nom ?>
			</span>
		</p>

		<!-- Modifier le profile -->
		<p class="w3-mobile">
			<a href="./profile.php" class="w3-block w3-button w3-border w3-text-theme">
				<i class="fa fa-pencil"></i>
				Modifier
			</a>
		</p>

	</div>
</div>
