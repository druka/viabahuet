<?php

function uploadImage( &$file ) {
	if( !getimagesize( $file["tmp_name"] ) )
		throw new Exception( "Le fichier n'est pas une image." );

	$nomImage = md5_file( $file['tmp_name'] );
	$cible    = "./images/" . $nomImage;

	if( !move_uploaded_file( $file['tmp_name'], $cible ) )
		throw new Exception(
			  "Erreur lors du téléversement de l'image "
			. $file['name']
		);

	return $nomImage;
}

?>
