<div id="sidebar" class="w3-col w3-mobile">
	<div class="w3-container w3-card w3-round w3-margin w3-white">

		<!-- L'image de profile. -->
		<div class="w3-center w3-margin-top w3-margin-bottom">
			<div
				class="vb-profilepic vb-large w3-border"
				style="background-image: url( './images/<?php echo $e->photo ?>' )">
			</div>
		</div>

		<!-- Le nom de l'entreprise -->
		<p class="w3-center">
			<strong><?php echo $e->nom_entreprise ?></strong>
		</p>

		<hr/>

		<!-- No. voie et voie -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $e->no_voie, ' ', $e->voie ?>
			</span>
		</p>

		<?php
			// La ville ou est situé l'entreprise.
			$v = Ville::selectById( $dbh, $e->id_ville );
		?>

		<!-- Ville -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $v->cp, ' ', $v->nom ?>
			</span>
		</p>

		<?php
			// Si l'entreprise à un site web, on l'affiche.
			if( $e->site_web ) :
		?>

		<!-- Site web -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-link fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<a href="\\<?php echo $e->site_web ?>">
					<?php echo $e->site_web ?>
				</a>
			</span>
		</p>

		<?php endif; // $e->site_web ?>

		<!-- Responsable -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-user fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $e->prenom, ' ', $e->nom ?>
			</span>
		</p>

		<!-- Mail -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $e->mail ?>
			</span>
		</p>

		<!-- Boutton ajouter offre -->
		<p>
			<a
				href="./offre-view.php?id_entreprise=<?php echo $e->id ?>"
				class="w3-block w3-button w3-border w3-text-theme w3-mobile">
				<i class="fa fa-plus"></i>
				Ajouter une offre
			</a>
		</p>

	</div>
</div>
