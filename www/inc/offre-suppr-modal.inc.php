<div class="w3-modal-content w3-card">
	<header class="w3-container w3-theme">
		<span
			class="w3-button w3-display-topright"
			onclick="$( '#offre-suppr-modal' ).hide()">
				&times;
			</span>
		<h2>Confirmation</h2>
	</header>
	<div class="w3-container">
		<p>Voulez-vous supprimer l'offre ?</p>
		<p class="w3-right">
			<a
				class="w3-button w3-white w3-border w3-text-theme"
				onclick="offreSupprOui( <?php echo $_GET['id'] ?> )">
				<i class="fa fa-trash"></i>
				Oui
			</a>
			<a
				class="w3-button w3-white w3-border w3-text-gray"
				onclick="$( '#offre-suppr-modal' ).hide()">
				<i class="fa fa-arrow-left"></i>
				Non
			</a>
		</p>
	</div>
</div>
