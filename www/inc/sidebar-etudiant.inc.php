<div id="sidebar" class="w3-col w3-mobile">
	<div class="w3-container w3-card w3-round w3-margin w3-white">

		<!-- L'image de profile. -->
		<div class="w3-center w3-margin-top w3-margin-bottom">
			<div
				class="vb-profilepic vb-large w3-border"
				style="background-image: url( './images/<?php echo $e->photo ?>' )">
			</div>
		</div>

		<!-- Le nom de l'étudiant -->
		<p class="w3-center">
			<strong><?php echo $e->prenom, ' ', $e->nom ?></strong>
		</p>

		<hr/>

		<?php
			// La formation de l'étudiant.
			$f = Formation::selectById( $dbh, $e->id_formation );
		?>

		<!-- Formation -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-graduation-cap fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $f->nom ?>
			</span>
		</p>

		<?php
			// La ville ou est situé l'étudiant.
			$v = Ville::selectById( $dbh, $e->id_ville );
		?>

		<!-- Ville -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-map-marker fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $v->cp, ' ', $v->nom ?>
			</span>
		</p>

		<!-- Mail -->
		<p class="w3-mobile">
			<span class="w3-cell">
				<i class="fa fa-envelope fa-fw w3-margin-right w3-text-theme"></i>
			</span>
			<span class="w3-cell">
				<?php echo $e->mail ?>
			</span>
		</p>

		<p>
			<?php
				// On vérifie que c'est pas nous même.
				if( $e->id != $u->id ) :

				// Récuperations des amis.
				$a = Ami::SelectById( $dbh, $u->id, $e->id );
				// Cet utilisateur est-il un ami ?
				if( $a ) :
			?>

			<!-- Boutton Supprimer un ami -->
			<a
				id="ami-suppr-<?php echo $e->id ?>"
				onclick="queryAmi( <?php echo $e->id ?>, 'Supprimer' )"
				class="w3-button w3-block w3-border w3-margin-bottom w3-mobile">
				<span class="label w3-text-gray">
					<i class="fa fa-minus"></i>
					Retirer des amis
				</span>
			</a>

			<?php
				// Sinon, on ne veut pas s'ajouter soit-meme en ami.
				elseif( $e->id != $u->id ) :
			?>

			<!-- Boutton Ajouter en ami -->
			<a
				id="ami-ajout-<?php echo $e->id ?>"
				onclick="queryAmi( <?php echo $e->id ?>, 'Ajouter' )"
				class="w3-button w3-block w3-text-theme w3-border w3-margin-bottom w3-mobile">
				<span class="label w3-text-theme">
					<i class="fa fa-plus"></i>
					Ajouter en ami
				</span>
			</a>

			<?php
				endif; // $a
				endif; // $e->id != $u->id
			?>

		</p>
	</div>
</div>
