<?php

require_once dirname(__FILE__) . '/utilisateur.inc.php';

class Eleve extends Utilisateur {
	private $id_formation;
	private $dna;
	private $preferences;
	private $long_deg;
	private $lat_deg;

	/**
	 * Constructeur de la classe Eleve.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			parent::__construct( $data );

			$this->id_formation = $data['id_Formation'];
			$this->dna					= $data['dna'					];
			$this->preferences  = $data['preferences' ];
			$this->long_deg     = $data['long_deg'    ];
			$this->lat_deg      = $data['lat_deg'     ];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'id_formation':
				$this->id_formation = (int)$value;
				break;
			case 'dna':
				$this->dna = (string)$value;
				break;
			case 'preferences':
				$this->preferences = (string)$value;
				break;
			case 'long_deg':
				$this->long_deg = (float)$value;
				break;
			case 'lat_deg':
				$this->lat_deg = (float)$value;
				break;
			default:
				parent::__set( $property, $value );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch ( $property ) {
			case 'id_formation':
				return $this->id_formation;
				break;
				case 'dna':
					return $this->dna;
					break;
			case 'preferences':
				return $this->preferences;
				break;
			case 'long_deg':
				return $this->long_deg;
				break;
			case 'lat_deg':
				return $this->lat_deg;
				break;
			default:
				return parent::__get( $property );
				break;
		}
	}

	/**
	 * Methode magique __tostring()
	 *
	 * @return string
	 */
	public function __tostring() {
		return '<pre>' . print_r( $this, true ) . '</pre>';
	}

	/**
	 * Methode statique de récuperation de tous les élèves.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @return array
	 * @throws Exception
	 */
	static public function selectAll( PDO $dbh ) {
		/** Le tableau d'élèves. */
		$eleves = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEleve\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Eleve.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $eleve ) {
				// Construction des élèves.
				$eleves[] = New Eleve( $eleve );
			}

			return $eleves;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de l'élève.
	 * @return array
	 * @throws Exception
	 */
	static public function selectById(
		PDO $dbh,
		int $id
	) {
		/** L'élève à retourner. */
		$eleve = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEleve\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Eleve.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tUtilisateur.id = :id\n"
				. "AND\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Construction de l'élève.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$eleve = New Eleve( $attrs );

			return $eleve;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $mail L'adresse e-mail de l'élève.
	 * @param string $mdp Le mot de passe de l'élève.
	 * @return array
	 * @throws Exception
	 */
	static public function selectByLogin(
		PDO $dbh,
		string $mail,
		string $mdp
	) {
		/** L'élève à retourner. */
		$eleve = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEleve\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Eleve.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tUtilisateur.mail = :mail\n"
				. "AND\n"
				. "\tUtilisateur.mdp = :mdp";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':mail', $mail );
			$sth->bindParam( ':mdp',  $mdp  );
			// Execution de la requête.
			$sth->execute();

			// Construction de l'élève.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$eleve = New Eleve( $attrs );

			return $eleve;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de recherche d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $like Le critère de recherche.
	 * @return array
	 * @throws Exception
	 */
	static public function like(
		PDO $dbh,
		string $like
	) {
		/** La liste des élèves à retourner. */
		$eleves = array();
		/** Critère de la clause LIKE. **/
		$like = '%' . $like . '%';

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEleve\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Eleve.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\t(nom LIKE :like\n"
				. "OR\n"
				. "\tprenom LIKE :like)\n"
				. "AND\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':like', $like );
			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $eleve ) {
				// Construction des élèves.
				$eleves[] = New Eleve( $eleve );
			}

			return $eleves;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique d'insertion d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param Eleve L'élève à insérer.
	 * @return array
	 * @throws Exception
	 */
	static public function insert(
		PDO $dbh,
		$eleve
	) {
		/*
		 * NOTE: Utilisateur::insert() accepte mixed pour le second
		 * paramètre (Eleve ou Entreprise).
		 *
		 * Eleve::insert() doit être compatible, donc on met mixed pour
		 * $eleve et on assert pour vérifier le type.
		 */
		assert( $eleve instanceof Eleve );

		try {
			// Début de la transaction.
			$dbh->beginTransaction();
			// Insertion dans la table Utilisateur.
			Utilisateur::insert( $dbh, $eleve );

			// Insertion dans la table Eleve.

			/** la requête SQL. */
			$sql
				= "INSERT INTO\n"
				. "\tEleve\n"
				. "VALUE("
				. "\tLAST_INSERT_ID(),\n"
				. "\t:id_formation,\n"
				. "\t:dna,\n"
				. "\t:preferences,\n"
				. "\t:long_deg,\n"
				. "\t:lat_deg\n"
				. ");";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id_formation', $eleve->id_formation );
			$sth->bindParam( ':dna', 					$eleve->dna					 );
			$sth->bindParam( ':preferences',  $eleve->preferences  );
			$sth->bindParam( ':long_deg',     $eleve->long_deg     );
			$sth->bindParam( ':lat_deg',      $eleve->lat_deg      );
			// Execution de la requête.
			$sth->execute();

			// Fin de la transaction.
			$dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			// Anulation de la transaction.
			$dbh->rollback();
			throw $e;
		}
	}

	/**
	 * Methode statique de mise à jour d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'élève à mettre à jour.
	 * @param Eleve $eleve Elève mis à jour.
	 * @throws Exception
	 */
	 static public function update(
			PDO $dbh,
			int $id,
			$eleve
		) {
		/*
		 * NOTE: Utilisateur::update() accepte mixed pour le second
		 * paramètre (Eleve ou Entreprise).
		 *
		 * Eleve::update() doit être compatible, donc on met mixed pour
		 * $eleve et on assert pour vérifier le type.
		 */
		assert( $eleve instanceof Eleve );

		try {
			// Début de la transaction.
			$dbh->beginTransaction();

			// màj dans la table Eleve.

			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tEleve\n"
				. "SET"
				. "\tid_formation = :id_formation,\n"
				. "\tdna = :dna,\n"
				. "\tpreferences = :preferences,\n"
				. "\tlong_deg = :long_deg,\n"
				. "\tlat_deg = :lat_deg\n"
				. "WHERE\n"
				. "\tid_Utilisateur = :id_Utilisateur;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id_formation',   $eleve->id_formation );
			$sth->bindParam( ':dna',   					$eleve->dna					 );
			$sth->bindParam( ':preferences',    $eleve->preferences  );
			$sth->bindParam( ':long_deg',       $eleve->long_deg     );
			$sth->bindParam( ':lat_deg',        $eleve->lat_deg      );
			$sth->bindParam( ':id_Utilisateur', $id                  );
			// Execution de la requête.
			$sth->execute();

			// màj dans la table Utilisateur.
			Utilisateur::update( $dbh, $id, $eleve );

			// Fin de la transaction.
			$dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			// Anulation de la transaction.
			$dbh->rollback();
			throw $e;
		}
	}

	/**
	 * Methode statique de suppression d'un élève.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de l'élève à supprimer.
	 * @throws Exception
	 */
	static public function delete(
		PDO $dbh,
		int $id
	) {
		try {
			// Début de la transaction.
			// $dbh->beginTransaction();
			//
			// // suppression dans la table Eleve.
			//
			// /** la requête SQL. */
			// $sql
			// 	= "DELETE FROM\n"
			// 	. "\tEleve\n"
			// 	. "WHERE\n"
			// 	. "\tid_Utilisateur = :id;";
			//
			// /** Handle sur la requête préparée. */
			// $sth = $dbh->prepare( $sql );
			//
			// // Binds.
			// $sth->bindParam( ':id', $id );
			// // Execution de la requête.
			// $sth->execute();

			// Suppression dans la table Utilisateur.
			Utilisateur::delete( $dbh, $id );

			// Fin de la transaction.
			// $dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			// Anulation de la transaction.
			// $dbh->rollback();
			throw $e;
		}
	}
};

?>
