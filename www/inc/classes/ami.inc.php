<?php

class Ami {
	private $id_Utilisateur_1;
	private $id_Utilisateur_2;
	private $accepter;

/**
 * Constructeur de la classe Ami.
 *
 * @param array $data Tableau associant propriétés / valeurs.
 * @return void
 */
public function __construct( array $data = null ) {
	if( is_array( $data ) ) {
		$this->id_Utilisateur_1 = $data['id_Utilisateur_1'];
		$this->id_Utilisateur_2 = $data['id_Utilisateur_2'];
		$this->accepter         = $data['accepter'        ];
	}
}

/**
 * Methode magique __set()
 *
 * @param string $property Nom de la propriété.
 * @param mixed $value Valeur à affecter à la propriété.
 * @return void
 */
public function __set( $property, $value ) {
	switch( $property ) {
		case 'id_Utilisateur_1':
			$this->id_Utilisateur_1 = (int)$value;
			break;
		case 'id_Utilisateur_2':
			$this->id_Utilisateur_2 = (int)$value;
			break;
		case 'accepter':
			$this->accepter = (int)$value;
			break;
		default:
			throw new Exception( "Propriété '$property' invalide." );
			break;
		}
	}

	/**
	* Methode magique __get()
	*
	* @param string $property Nom de la propriété à atteindre.
	* @return mixed|null
	*/
	public function __get( $property ) {
		switch( $property ) {
			case 'id_Utilisateur_1':
			return $this->id_Utilisateur_1;
			break;
			case 'id_Utilisateur_2':
			return $this->id_Utilisateur_2;
			break;
			case 'accepter':
			return $this->accepter;
			break;
		default:
			throw new Exception( "Propriété '$property' invalide." );
			break;
		}
	}
		/**
		* Methode magique __tostring()
		*
		* @return string
		*/
		public function __tostring() {
			return '<pre>' . print_r( $this, true ) . '</pre>';
		}

		/**
		* Methode statique de récuperation de toute les amis.
		*
		* @param PDO $dbh Handle sur la connexion à la base de donnée.
		* @return array
		* @throws Exception
		*/
		static public function selectAll( PDO $dbh ) {
			/** Le tableau des offres. */
			$amis = array();

			try {
				/** La requête SQL. */
				$sql
					= "SELECT\n"
					. "\t*\n"
					. "FROM\n"
					. "\tAmi";

				/** Handle sur la requête préparée. */
				$sth = $dbh->prepare( $sql );

				// Execution de la requête.
				$sth->execute();

				foreach( $sth->fetchAll() as $ami ) {
					// Construction des offres.
					$amis[] = New ami( $ami );
				}

				return $amis;
			}

			// Si quelque chose c'est mal passé :
			catch( Exception $e ) {
				throw $e;
			}
		}

		/**
		* Methode statique de récuperation des amis.
		*
		* @param PDO $dbh Handle sur la connexion à la base de donnée.
		* @param int $id_utilisateur_1 L'identifiant de l'utilisateur.
		* @param int $id_utilisateur_2 L'identifiant de l'ami.
		* @return array
		* @throws Exception
		*/
		static public function selectById(
			PDO $dbh,
			int $id_utilisateur_1,
			$id_utilisateur_2 = null
		) {
			/** Le tableau des amis. */
			$amis = array();

			try {
				/** La requête SQL. */
				$sql
					= "SELECT\n"
					. "\t*\n"
					. "FROM\n"
					. "\tAmi\n"
					. "WHERE\n"
					. "\t(id_Utilisateur_1 = :id_utilisateur_1\n"
					. "OR\n"
					. "\tid_Utilisateur_2 = :id_utilisateur_1)\n";

				if( $id_utilisateur_2 )
					$sql
						.= "AND\n"
						.  "\t(id_Utilisateur_1 = :id_utilisateur_2\n"
						.  "OR\n"
						.  "\tid_Utilisateur_2 = :id_utilisateur_2)\n";

				/** Handle sur la requête préparée. */
				$sth = $dbh->prepare( $sql );

				// Binds.
				$sth->bindParam( ':id_utilisateur_1', $id_utilisateur_1 );

				if( $id_utilisateur_2 )
					$sth->bindParam( ':id_utilisateur_2', $id_utilisateur_2 );

				// Execution de la requête.
				$sth->execute();

				foreach( $sth->fetchAll() as $ami ) {
					// Construction des amis.
					$amis[] = New Ami( $ami );
				}

				return $amis;
			}

			// Si quelque chose c'est mal passé :
			catch( Exception $e ) {
				throw $e;
			}
		}

		/**
		* Methode statique d'insertion d'un requête d'ami.
		*
		* @param PDO $dbh Handle sur la connexion à la base de donnée.
		* @param int les identifiants des deux utilisateurs.
		* @param Ami L'offre à insérer.
		* @throws Exception
		*/
		static public function update(
			PDO $dbh,
			ami $ami
		) {
			try {
				/** la requête SQL. */
				$sql
					= "UPDATE\n"
					. "\tAmi\n"
					. "SET"
					. "\taccepter = 1\n"
					. "WHERE\n"
					. "\tid_Utilisateur_1 = :id_Utilisateur_1\n"
					. "AND \n"
					. "\tid_Utilisateur_2 = :id_Utilisateur_2;\n";

				/** Handle sur la requête préparée. */
				$sth = $dbh->prepare( $sql );

				// Binds.
				$sth->bindParam( ':id_Utilisateur_1', $ami->id_Utilisateur_1 );
				$sth->bindParam( ':id_Utilisateur_2', $ami->id_Utilisateur_2 );

				// Execution de la requête.
				$sth->execute();
			}

			// Si quelque chose c'est mal passé :
			catch( Exception $e ) {
				throw $e;
			}
		}

		/**
		* Methode statique d'acceptation entre ami.
		*
		* @param PDO $dbh Handle sur la connexion à la base de donnée.
		* @param Ami L'ami à insérer.
		* @throws Exception
		*/
		static public function insert(
			PDO $dbh,
			ami $ami
		) {
			try {
				/** la requête SQL. */
				$sql
					= "INSERT INTO\n"
					. "\tAmi\n"
					. "VALUE("
					. "\t:id_Utilisateur_1,\n"
					. "\t:id_Utilisateur_2,\n"
					. "\t:accepter\n"
					. ");";

				/** Handle sur la requête préparée. */
				$sth = $dbh->prepare( $sql );

				// Binds.
				$sth->bindParam( ':id_Utilisateur_1', $ami->id_Utilisateur_1 );
				$sth->bindParam( ':id_Utilisateur_2', $ami->id_Utilisateur_2 );
				$sth->bindParam( ':accepter',         $ami->accepter );

				// Execution de la requête.
				$sth->execute();
			}

			// Si quelque chose c'est mal passé :
			catch( Exception $e ) {
				throw $e;
			}
		}

		/**
		* Methode statique de suppression d'un ami,
		* ou d'un rejet d'ajout ami.
		*
		* @param PDO $dbh Handle sur la connexion à la base de donnée.
		* @param int $id_utilisateur_1 L'id de l'utilsiateur.
		* @param int $id_utilisateur_2 L'id de l'ami.
		* @throws Exception
		*/
		static public function delete(
			PDO $dbh,
			int $id_utilisateur_1,
			int $id_utilisateur_2
		) {
		try {
			/** la requête SQL. */
			$sql
				= "DELETE FROM\n"
				. "\tAmi\n"
				. "WHERE\n"
				. "\tid_Utilisateur_1 = :id_Utilisateur_1\n"
				. "AND\n"
				. "\tid_Utilisateur_2 = :id_Utilisateur_2;\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id_Utilisateur_1', $id_utilisateur_1 );
			$sth->bindParam( ':id_Utilisateur_2', $id_utilisateur_2 );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			throw $e;
		}
	}
};

?>
