<?php

class Offre{
	private $id;
	private $titre;
	private $description;
	private $debut;
	private $fin;
	private $salaire_hr_ht;
	private $publication;
	private $note;
	private $commentaire;
	private $report;
	private $id_utilisateur;
	private $id_entreprise;
	private $id_eleve;

	/**
	 * Constructeur de la classe Offre.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			$this->id             = $data['id'            ];
			$this->titre          = $data['titre'         ];
			$this->description    = $data['description'   ];
			$this->debut          = $data['debut'         ];
			$this->fin            = $data['fin'           ];
			$this->salaire_hr_ht  = $data['salaire_hr_ht' ];
			$this->publication    = $data['publication'   ];
			$this->note           = $data['note'          ];
			$this->commentaire    = $data['commentaire'   ];
			$this->report         = $data['report'        ];
			$this->id_utilisateur = $data['id_Utilisateur'];
			$this->id_entreprise  = $data['id_Entreprise' ];
			$this->id_eleve       = $data['id_Eleve'      ];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'id':
				$this->id = (int)$value;
				break;
			case 'titre':
				$this->titre = (string)$value;
				break;
			case 'description':
				$this->description = (string)$value;
				break;
			case 'debut':
				$this->debut = (string)$value;
				break;
			case 'fin':
				$this->fin = (string)$value;
				break;
			case 'salaire_hr_ht':
				$this->salaire_hr_ht = (string)$value;
				break;
			case 'publication':
				$this->publication = (string)$value;
				break;
			case 'note':
				$this->note = (int)$value;
				break;
			case 'commentaire':
				$this->commentaire = (string)$value;
				break;
			case 'report':
				$this->report = (int)$value;
				break;
			case 'id_utilisateur':
				$this->id_utilisateur = (int)$value;
				break;
			case 'id_entreprise':
				$this->id_entreprise = (int)$value;
				break;
			case 'id_eleve':
				$this->id_eleve = (int)$value;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch( $property ) {
			case 'id':
				return $this->id;
				break;
			case 'titre':
				return $this->titre;
				break;
			case 'description':
				return $this->description;
				break;
			case 'debut':
				return $this->debut;
				break;
			case 'fin':
				return $this->fin;
				break;
			case 'salaire_hr_ht':
				return $this->salaire_hr_ht;
				break;
			case 'publication':
				return $this->publication;
				break;
			case 'note':
				return $this->note;
				break;
			case 'commentaire':
				return $this->commentaire;
				break;
			case 'report':
				return $this->report;
				break;
			case 'id_utilisateur':
				return $this->id_utilisateur;
				break;
			case 'id_entreprise':
				return $this->id_entreprise;
				break;
			case 'id_eleve':
				return $this->id_eleve;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __tostring()
	 *
	 * @return string
	 */
	public function __tostring() {
		return '<pre>' . print_r( $this, true ) . '</pre>';
	}

	/**
	 * Methode statique de récuperation de toute les offres.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @return array
	 * @throws Exception
	 */
	static public function selectAll( PDO $dbh ) {
		/** Le tableau des offres. */
		$offres = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tOffre\n"
				. "WHERE\n"
				. "\tvalide = 1";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $offre ) {
				// Construction des offres.
				$offres[] = New Offre( $offre );
			}

			return $offres;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'une offre.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de l'offre.
	 * @return Offre
	 * @throws Exception
	 */
	static public function selectById(
		PDO $dbh,
		int $id
	) {
		/** L'offre à retourner. */
		$offre = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tOffre\n"
				. "WHERE\n"
				. "\tvalide = 1\n"
				. "AND\n"
				. "\tid = :id";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Construction de l'offre.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$offre = New Offre( $attrs );

			return $offre;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation des offre d'une entreprise..
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id_entreprise L'identifiant de l'entreprise.
	 * @return array
	 * @throws Exception
	 */
	static public function selectByEntreprise(
		PDO $dbh,
		int $id_entreprise
	) {
		/** Le tableau des offres. */
		$offres = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tOffre\n"
				. "WHERE\n"
				. "\tvalide = 1\n"
				. "AND\n"
				. "\tid_Entreprise = :id_entreprise";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id_entreprise', $id_entreprise );
			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $offre ) {
				// Construction des offres.
				$offres[] = New Offre( $offre );
			}

			return $offres;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de recherche d'une offre.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $like Le critère de recherche.
	 * @param int $id_entreprise L'id de l'entreprise.
	 * @param int $id_eleve L'id de l'élève.
	 * @return array
	 * @throws Exception
	 */
	static public function like(
		PDO $dbh,
		$id_utilisateur,
		$id_entreprise,
		$id_eleve,
		string $like
	) {
		/** La liste des élèves à retourner. */
		$offres = array();
		/** Critère de la clause LIKE. **/
		$like = '%' . $like . '%';

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tOffre\n"
				. "WHERE\n"
				. "\t(titre LIKE :like\n"
				. "OR\n"
				. "\tdescription LIKE :like)\n"
				. "AND\n"
				. "\tvalide = 1\n";

				// Recherche pour un utilisateur particulier.
				if( $id_utilisateur )
					$sql
						.= "AND\n"
						.	 "\tid_Utilisateur=:id_utilisateur\n";

				// Recherche pour une entreprise particulière.
				if( $id_entreprise )
					$sql
						.= "AND\n"
						.	 "\tid_Entreprise=:id_entreprise\n";

				// Recherche pour un élève particulier.
				if( $id_eleve )
					$sql
						.= "AND\n"
						.	 "\tid_Eleve=:id_eleve\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':like', $like );

			if( $id_utilisateur )
				$sth->bindParam( ':id_utilisateur', $id_utilisateur );
			if( $id_entreprise )
				$sth->bindParam( ':id_entreprise', $id_entreprise );
			if( $id_eleve )
				$sth->bindParam( ':id_eleve', $id_eleve );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $offre ) {
				// Construction des offres.
				$offres[] = New Offre( $offre );
			}

			return $offres;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique d'insertion d'un offre.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param Offre L'offre à insérer.
	 * @throws Exception
	 */
	static public function insert(
		PDO $dbh,
		Offre $offre
	) {
		try {
			/** la requête SQL. */
			$sql
				= "INSERT INTO\n"
				. "\tOffre\n"
				. "VALUE("
				. "\tLAST_INSERT_ID(),\n"
				. "\t:titre,\n"
				. "\t:description,\n"
				. "\t:debut,\n"
				. "\t:fin,\n"
				. "\t:salaire_hr_ht,\n"
				. "\t:publication,\n"
				. "\t:note,\n"
				. "\t:commentaire,\n"
				. "\t:report,\n"
				. "\t:id_utilisateur,\n"
				. "\t:id_entreprise,\n"
				. "\t:id_eleve,\n"
				. "\t1\n"
				. ");";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':titre',          $offre->titre          );
			$sth->bindParam( ':description',    $offre->description    );
			$sth->bindParam( ':debut',          $offre->debut          );
			$sth->bindParam( ':fin',            $offre->fin            );
			$sth->bindParam( ':salaire_hr_ht',  $offre->salaire_hr_ht  );
			$sth->bindParam( ':publication',    $offre->publication    );
			$sth->bindParam( ':note',           $offre->note           );
			$sth->bindParam( ':commentaire',    $offre->commentaire    );
			$sth->bindParam( ':report',         $offre->report         );
			$sth->bindParam( ':id_utilisateur', $offre->id_utilisateur );
			$sth->bindParam( ':id_entreprise',  $offre->id_entreprise  );
			$sth->bindParam( ':id_eleve',       $offre->id_eleve       );

			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de mise à jour d'un utilisateur.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'utilisateur à mettre à jour.
	 * @param Offre $offre L'offre mise à jour.
	 * @throws Exception
	 */
	static public function update(
		PDO $dbh,
		int $id,
		Offre $offre
	) {
		try {
			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tOffre\n"
				. "SET"
				. "\ttitre = :titre,\n"
				. "\tdescription = :description,\n"
				. "\tdebut = :debut,\n"
				. "\tfin = :fin,\n"
				. "\tsalaire_hr_ht = :salaire_hr_ht,\n"
				. "\tpublication = :publication,\n"
				. "\tnote = :note,\n"
				. "\tcommentaire = :commentaire,\n"
				. "\treport = :report,\n"
				. "\tid_utilisateur = :id_utilisateur,\n"
				. "\tid_entreprise = :id_entreprise,\n"
				. "\tid_eleve = :id_eleve\n"
				. "WHERE\n"
				. "\tid = :id;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id',             $offre->id             );
			$sth->bindParam( ':titre',          $offre->titre          );
			$sth->bindParam( ':description',    $offre->description    );
			$sth->bindParam( ':debut',          $offre->debut          );
			$sth->bindParam( ':fin',            $offre->fin            );
			$sth->bindParam( ':salaire_hr_ht',  $offre->salaire_hr_ht  );
			$sth->bindParam( ':publication',    $offre->publication    );
			$sth->bindParam( ':note',           $offre->note           );
			$sth->bindParam( ':commentaire',    $offre->commentaire    );
			$sth->bindParam( ':report',         $offre->report         );
			$sth->bindParam( ':id_utilisateur', $offre->id_utilisateur );
			$sth->bindParam( ':id_entreprise',  $offre->id_entreprise  );
			$sth->bindParam( ':id_eleve',       $offre->id_eleve       );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de suppression d'une offre.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'offre à supprimer.
	 * @throws Exception
	 */
	static public function delete(
		PDO $dbh,
		int $id
	) {
		try {
			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tOffre\n"
				. "SET"
				. "\tvalide = 0\n"
				. "WHERE\n"
				. "\tid = :id;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			throw $e;
		}
	}
};

?>
