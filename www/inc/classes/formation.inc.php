<?php

class Formation{
	private $id;
	private $nom;

	/**
	 * Constructeur de la classe Formation.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			$this->id  = $data['id'];
			$this->nom = $data['nom'];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'id':
				$this->id = (int)$value;
				break;
			case 'nom':
				$this->nom = (string)$value;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch( $property ) {
			case 'id':
				return $this->id;
				break;
			case 'nom':
				return $this->nom;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __tostring()
	 *
	 * @return string
	 */
	public function __tostring() {
		return '<pre>' . print_r( $this, true ) . '</pre>';
	}

	/**
	 * Methode statique de récuperation de toute les formations.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @return array
	 * @throws Exception
	 */
	static public function selectAll( PDO $dbh ) {
		/** Le tableau des formations. */
		$formations = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tFormation";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $formation ) {
				// Construction des formations.
				$formations[] = New Formation( $formation );
			}

			return $formations;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'une formation.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de la formation.
	 * @return array
	 * @throws Exception
	 */
	static public function selectById(
		PDO $dbh,
		int $id
	) {
		/** La formation à retourner. */
		$formation = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tFormation\n"
				. "WHERE\n"
				. "\tid = :id";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Construction de la formation.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$formation = New Formation( $attrs );

			return $formation;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}
};

?>
