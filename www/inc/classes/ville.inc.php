<?php

class Ville implements \JsonSerializable {
	private $id;
	private $nom;
	private $cp;
	private $longitude;
	private $latitude;

	/**
	 * Constructeur de la classe Ville.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			$this->id          = $data['id'];
			$this->nom         = $data['nom'];
			$this->cp          = $data['cp'];
			$this->longitude   = $data['longitude'];
			$this->latitude    = $data['latitude'];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'id':
				$this->id = (int)$value;
				break;
			case 'nom':
				$this->nom = (string)$value;
				break;
			case 'cp':
				$this->cp = (string)$value;
				break;
			case 'longitude':
				$this->longitude = (float)$value;
				break;
			case 'latitude':
				$this->latitude = (float)$value;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch( $property ) {
			case 'id':
				return $this->id;
				break;
			case 'nom':
				return $this->nom;
				break;
			case 'cp':
				return $this->cp;
				break;
			case 'longitude':
				return $this->longitude;
				break;
			case 'latitude':
				return $this->latitude;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __tostring()
	 *
	 * @return string
	 */
	public function __tostring() {
		return '<pre>' . print_r( $this, true ) . '</pre>';
	}

	/**
	 * Fonction de conversion de l'objet en JSON.
	 *
	 * @return string
	 */
	public function jsonSerialize() {
		return get_object_vars( $this );
  }

	/**
	 * Methode statique de récuperation de toute les villes.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @return array
	 * @throws Exception
	 */
	static public function selectAll( PDO $dbh ) {
		/** Le tableau d'élèves. */
		$villes = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tvVille";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $ville ) {
				// Construction des villes.
				$villes[] = New Ville( $ville );
			}

			return $villes;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'une ville.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de la ville.
	 * @return array
	 * @throws Exception
	 */
	static public function selectById(
		PDO $dbh,
		int $id
	) {
		/** La ville à retourner. */
		$ville = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tvVille\n"
				. "WHERE\n"
				. "\tid = :id";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Construction de la ville.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$ville = New Ville( $attrs );

			return $ville;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de recherche d'une ville.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $like Le critère de recherche.
	 * @return array
	 * @throws Exception
	 */
	static public function like(
		PDO $dbh,
		string $like
	) {
		/** La liste des villes à retourner. */
		$villes = array();
		/** Critère de la clause LIKE. **/
		$like = $like . '%';

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tvVille\n"
				. "WHERE\n"
				. "\tnom LIKE :like\n"
				. "ORDER BY\n"
				. "\nnom ASC, cp ASC LIMIT 25";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':like', $like );
			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $ville ) {
				// Construction des villes.
				$villes[] = New Ville( $ville );
			}

			return $villes;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}
};

?>
