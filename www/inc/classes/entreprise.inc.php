<?php

require_once dirname(__FILE__) . '/utilisateur.inc.php';

class Entreprise extends Utilisateur {
	private $nom_entreprise;
	private $desc_entreprise;
	private $site_web;

	/**
	 * Constructeur de la classe Entreprise.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			parent::__construct( $data );

			$this->nom_entreprise  = $data['nom_entreprise' ];
			$this->desc_entreprise = $data['desc_entreprise'];
			$this->site_web        = $data['site_web'       ];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'nom_entreprise':
				$this->nom_entreprise = (string)$value;
				break;
			case 'desc_entreprise':
				$this->desc_entreprise = (string)$value;
				break;
			case 'site_web':
				$this->site_web = (string)$value;
				break;
			default:
				parent::__set( $property, $value );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch ( $property ) {
			case 'nom_entreprise':
				return $this->nom_entreprise;
				break;
			case 'desc_entreprise':
				return $this->desc_entreprise;
				break;
			case 'site_web':
				return $this->site_web;
				break;
			default:
				return parent::__get( $property );
				break;
		}
	}

	/**
	 * Methode magique __tostring()
	 *
	 * @return string
	 */
	public function __tostring() {
		return
			'<pre>' . print_r( $this, true ) . '</pre>';
	}

	/**
	 * Methode statique de récuperation de tous les entreprises.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @return array
	 * @throws Exception
	 */
	static public function selectAll( PDO $dbh ) {
		/** Le tableau des entreprises. */
		$entreprises = array();

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEntreprise\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Entreprise.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $entreprise ) {
				// Construction des entreprises.
				$entreprises[] = New Entreprise( $entreprise );
			}

			return $entreprises;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de l'entreprise.
	 * @return array
	 * @throws Exception
	 */
	static public function selectById(
		PDO $dbh,
		int $id
	) {
		/** L'entreprise à retourner. */
		$entreprise = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEntreprise\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Entreprise.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tUtilisateur.id = :id\n"
				. "AND\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Construction de l'entreprise.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$entreprise = New Entreprise( $attrs );

			return $entreprise;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de récuperation d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $mail L'adresse e-mail de l'entreprise.
	 * @param string $mdp Le mot de passe de l'entreprise.
	 * @return array
	 * @throws Exception
	 */
	static public function selectByLogin(
		PDO $dbh,
		string $mail,
		string $mdp
	) {
		/** L'entreprise à retourner. */
		$entreprise = null;

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEntreprise\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Entreprise.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tUtilisateur.mail = :mail\n"
				. "AND\n"
				. "\tUtilisateur.mdp = :mdp";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':mail', $mail );
			$sth->bindParam( ':mdp',  $mdp  );
			// Execution de la requête.
			$sth->execute();

			// Construction de l'entreprise.
			$attrs = $sth->fetch();

			if( is_array( $attrs ) )
				$entreprise = New Entreprise( $attrs );

			return $entreprise;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de recherche d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param string $like Le critère de recherche.
	 * @return array
	 * @throws Exception
	 */
	static public function like(
		PDO $dbh,
		string $like
	) {
		/** La liste des entreprises à retourner. */
		$entreprises = array();
		/** Critère de la clause LIKE. **/
		$like = '%' . $like . '%';

		try {
			/** La requête SQL. */
			$sql
				= "SELECT\n"
				. "\t*\n"
				. "FROM\n"
				. "\tEntreprise\n"
				. "JOIN\n"
				. "\tUtilisateur\n"
				. "\tON Entreprise.id_Utilisateur = Utilisateur.id\n"
				. "WHERE\n"
				. "\tnom_entreprise LIKE :like\n"
				. "AND\n"
				. "\tvalide = 1\n";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':like', $like );
			// Execution de la requête.
			$sth->execute();

			foreach( $sth->fetchAll() as $entreprise ) {
				// Construction des entreprises.
				$entreprises[] = New Entreprise( $entreprise );
			}

			return $entreprises;
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique d'insertion d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param Entreprise L'entreprise à insérer.
	 * @return array
	 * @throws Exception
	 */
	static public function insert(
		PDO $dbh,
		$entreprise
	) {
		/*
		 * NOTE: Utilisateur::insert() accepte mixed pour le second
		 * paramètre (Eleve ou Entreprise).
		 *
		 * Entreprise::insert() doit être compatible, donc on met mixed pour
		 * $entreprise et on assert pour vérifier le type.
		 */
		assert( $entreprise instanceof Entreprise );

		try {
			// Début de la transaction.
			$dbh->beginTransaction();
			// Insertion dans la table Utilisateur.
			parent::insert( $dbh, $entreprise );

			// Insertion dans la table Entreprise.

			/** la requête SQL. */
			$sql
				= "INSERT INTO\n"
				. "\tEntreprise\n"
				. "VALUE("
				. "\tLAST_INSERT_ID(),\n"
				. "\t:nom_entreprise,\n"
				. "\t:desc_entreprise\n"
				. "\t:site_web,\n"
				. ");";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':nom_entreprise', $entreprise->nom_entreprise );
			$sth->bindParam( ':desc_entreprise',$entreprise->desc_entreprise);
			$sth->bindParam( ':site_web',       $entreprise->site_web       );
			// Execution de la requête.
			$sth->execute();

			// Fin de la transaction.
			$dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			// Anulation de la transaction.
			$dbh->rollback();
			throw $e;
		}
	}

	/**
	 * Methode statique de mise à jour d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'entreprise à mettre à jour.
	 * @param Entreprise $entreprise Entreprise mise à jour.
	 * @throws Exception
	 */
	 static public function update(
			PDO $dbh,
			int $id,
			$entreprise
		) {
		/*
		 * NOTE: Utilisateur::update() accepte mixed pour le second
		 * paramètre (Eleve ou Entreprise).
		 *
		 * Entreprise::update() doit être compatible, donc on met mixed pour
		 * $entreprise et on assert pour vérifier le type.
		 */
		assert( $entreprise instanceof Entreprise );

		try {
			// Début de la transaction.
			$dbh->beginTransaction();

			// màj dans la table Entreprise.

			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tEntreprise\n"
				. "SET"
				. "\tnom_entreprise = :nom_entreprise,\n"
				. "\tsite_web = :site_web,\n"
				. "\tdesc_entreprise = :desc_entreprise\n"
				. "WHERE\n"
				. "\tid_Utilisateur = :id_Utilisateur;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':nom_entreprise',  $entreprise->nom_entreprise  );
			$sth->bindParam( ':site_web',        $entreprise->site_web        );
			$sth->bindParam( ':desc_entreprise', $entreprise->desc_entreprise );
			$sth->bindParam( ':id_Utilisateur',  $id                          );
			// Execution de la requête.
			$sth->execute();

			// màj dans la table Utilisateur.
			Utilisateur::update( $dbh, $id, $entreprise );

			// Fin de la transaction.
			$dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			// Anulation de la transaction.
			$dbh->rollback();
			throw $e;
		}
	}

	/**
	 * Methode statique de suppression d'une entreprise.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id L'identifiant de l'entreprise à supprimer.
	 * @throws Exception
	 */
	static public function delete(
		PDO $dbh,
		int $id
	) {
		try {
			// Début de la transaction.
			$dbh->beginTransaction();

			// suppression dans la table Entreprise.

			/** la requête SQL. */
			$sql
				= "DELETE FROM\n"
				. "\tEntreprise\n"
				. "WHERE\n"
				. "\tid_Utilisateur = :id;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();

			// Suppression dans la table Utilisateur.
			Utilisateur::delete( $dbh, $id );

			// Fin de la transaction.
			$dbh->commit();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			// Anulation de la transaction.
			$dbh->rollback();
			throw $e;
		}
	}
};

?>
