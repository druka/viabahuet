<?php

abstract class Utilisateur {
	protected $id;
	protected $nom;
	protected $prenom;
	protected $mail;
	protected $mdp;
	protected $photo;
	protected $tel;
	protected $no_voie;
	protected $voie;
	protected $termes;
	protected $id_ville;

	/**
	 * Constructeur de la classe Utilisateur.
	 *
	 * @param array $data Tableau associant propriétés / valeurs.
	 * @return void
	 */
	public function __construct( array $data = null ) {
		if( is_array( $data ) ) {
			$this->id       = $data['id'      ];
			$this->nom      = $data['nom'     ];
			$this->prenom   = $data['prenom'  ];
			$this->mail     = $data['mail'    ];
			$this->mdp      = $data['mdp'     ];
			$this->photo    = $data['photo'   ];
			$this->tel      = $data['tel'     ];
			$this->no_voie  = $data['no_voie' ];
			$this->voie     = $data['voie'    ];
			$this->termes   = $data['termes'  ];
			$this->id_ville = $data['id_Ville'];
		}
	}

	/**
	 * Methode magique __set()
	 *
	 * @param string $property Nom de la propriété.
	 * @param mixed $value Valeur à affecter à la propriété.
	 * @return void
	 */
	public function __set( $property, $value ) {
		switch( $property ) {
			case 'id':
				$this->id = (int)$value;
				break;
			case 'nom':
				$this->nom = (string)$value;
				break;
			case 'prenom':
				$this->prenom = (string)$value;
				break;
			case 'mail':
				$this->mail = (string)$value;
				break;
			case 'mdp':
				$this->mdp = (string)$value;
				break;
			case 'photo':
				$this->photo = (string)$value;
				break;
			case 'tel':
				$this->tel = (string)$value;
				break;
			case 'no_voie':
				$this->no_voie = (string)$value;
				break;
			case 'voie':
				$this->voie = (string)$value;
				break;
			case 'termes':
				$this->termes = (int)$value;
				break;
			case 'id_ville':
				$this->id_ville = (int)$value;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode magique __get()
	 *
	 * @param string $property Nom de la propriété à atteindre.
	 * @return mixed|null
	 */
	public function __get( $property ) {
		switch ( $property ) {
			case 'id':
				return $this->id;
				break;
			case 'nom':
				return $this->nom;
				break;
			case 'prenom':
				return $this->prenom;
				break;
			case 'mail':
				return $this->mail;
				break;
			case 'mdp':
				return $this->mdp;
				break;
			case 'photo':
				return $this->photo;
				break;
			case 'tel':
				return $this->tel;
				break;
			case 'no_voie':
				return $this->no_voie;
				break;
			case 'voie':
				return $this->voie;
				break;
			case 'termes':
				return $this->termes;
				break;
			case 'id_ville':
				return $this->id_ville;
				break;
			default:
				throw new Exception( "Propriété '$property' invalide." );
				break;
		}
	}

	/**
	 * Methode statique d'insertion d'un utilisateur.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param mixed L'utilisateur à insérer.
	 * @throws Exception
	 */
	static protected function insert(
		PDO $dbh,
		$utilisateur
	) {
		try {
			/** la requête SQL. */
			$sql
				= "INSERT INTO\n"
				. "\tUtilisateur\n"
				. "VALUE("
				. "\tNULL,\n"
				. "\t:nom,\n"
				. "\t:prenom,\n"
				. "\t:mail,\n"
				. "\t:mdp,\n"
				. "\t:photo,\n"
				. "\t:tel,\n"
				. "\t:no_voie,\n"
				. "\t:voie,\n"
				. "\t:termes,\n"
				. "\t:id_ville,\n"
				. "\t1\n"
				. ");";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':nom',      $utilisateur->nom      );
			$sth->bindParam( ':prenom',   $utilisateur->prenom   );
			$sth->bindParam( ':mail',     $utilisateur->mail     );
			$sth->bindParam( ':mdp',      $utilisateur->mdp      );
			$sth->bindParam( ':photo',    $utilisateur->photo    );
			$sth->bindParam( ':tel',      $utilisateur->tel      );
			$sth->bindParam( ':no_voie',  $utilisateur->no_voie  );
			$sth->bindParam( ':voie',     $utilisateur->voie     );
			$sth->bindParam( ':termes',   $utilisateur->termes   );
			$sth->bindParam( ':id_ville', $utilisateur->id_ville );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de mise à jour d'un utilisateur.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'utilisateur à mettre à jour.
	 * @param mixed $utilisateur Utilisateur mis à jour.
	 * @throws Exception
	 */
	static protected function update(
		PDO $dbh,
		int $id,
		$utilisateur
	) {
		try {
			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tUtilisateur\n"
				. "SET"
				. "\tnom = :nom,\n"
				. "\tprenom = :prenom,\n"
				. "\tmail = :mail,\n"
				. "\tmdp = :mdp,\n"
				. "\tphoto = :photo,\n"
				. "\ttel = :tel,\n"
				. "\tno_voie = :no_voie,\n"
				. "\tvoie = :voie,\n"
				. "\ttermes = :termes,\n"
				. "\tid_ville = :id_ville\n"
				. "WHERE\n"
				. "\tid = :id;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':nom',      $utilisateur->nom      );
			$sth->bindParam( ':prenom',   $utilisateur->prenom   );
			$sth->bindParam( ':mail',     $utilisateur->mail     );
			$sth->bindParam( ':mdp',      $utilisateur->mdp      );
			$sth->bindParam( ':photo',    $utilisateur->photo    );
			$sth->bindParam( ':tel',      $utilisateur->tel      );
			$sth->bindParam( ':no_voie',  $utilisateur->no_voie  );
			$sth->bindParam( ':voie',     $utilisateur->voie     );
			$sth->bindParam( ':termes',   $utilisateur->termes   );
			$sth->bindParam( ':id_ville', $utilisateur->id_ville );
			$sth->bindParam( ':id',       $id                    );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			throw $e;
		}
	}

	/**
	 * Methode statique de suppression d'un utilisateur.
	 *
	 * @param PDO $dbh Handle sur la connexion à la base de donnée.
	 * @param int $id Identifiant de l'utilisateur à supprimer.
	 * @throws Exception
	 */
	static protected function delete(
		PDO $dbh,
		int $id
	) {
		try {
			/** la requête SQL. */
			$sql
				= "UPDATE\n"
				. "\tUtilisateur\n"
				. "SET"
				. "\tvalide = 0\n"
				. "WHERE\n"
				. "\tid = :id;";

			/** Handle sur la requête préparée. */
			$sth = $dbh->prepare( $sql );

			// Binds.
			$sth->bindParam( ':id', $id );
			// Execution de la requête.
			$sth->execute();
		}

		// Si quelque chose c'est mal passé :
		catch( PDOException $e ) {
			throw $e;
		}
	}
};

?>
