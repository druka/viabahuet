<!-- Général -->
<fieldset class="w3-section">
	<legend><b>Général</b></legend>
	<div class="w3-row-padding">

		<!-- Prénom -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Prénom :</label>
			<input
				type="text"
				name="prenom"
				value="<?php echo $u->prenom ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- Nom -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Nom :</label>
			<input
				type="text"
				name="nom"
				value="<?php echo $u->nom ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>
	</div>
</fieldset>

<!-- Photo de profile -->
<fieldset class="w3-center">
	<legend><b>Photo de pofile</b></legend>
	<input type="hidden" name="photo" value="<?php echo $u->photo ?>"/>
	<p>
		<input type="file" name="photo_up"/>
	</p>
	<?php if( $u->photo != null ) : ?>
	<p>
		<label>
			<input type="checkbox" name="photo_suppr" value="">
			Supprimer
		</label>
	</p>
	<p>
		<a href="./images/<?php echo $u->photo ?>">
			<img class="w3-border w3-border-theme" src="./images/<?php echo $u->photo ?>" width="150" />
		</a>
	</p>
	<?php endif; ?>
</fieldset>

<!-- Informations de connexion -->
<fieldset class="w3-section">
	<legend><b>Informations de connexion</b></legend>
	<div class="w3-row-padding">

		<!-- E-mail -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>E-mail :</label>
			<input
				type="text"
				name="mail"
				value="<?php echo $u->mail ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- E-mail (confirmation) -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>E-mail (confirmation) :</label>
			<input
				type="text"
				name="mail-check"
				value="<?php echo $u->mail ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- Mot de passe -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Mot de passe :</label>
			<input
				type="password"
				name="mdp"
				value="<?php echo $u->mdp ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- Mot de passe (Confirmation) -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Mot de passe (confirmation) :</label>
			<input
				type="password"
				name="mdp-check"
				value="<?php echo $u->mdp ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

	</div>
</fieldset>

<?php if( $u instanceof Eleve ) : ?>

<!-- Informations de l'étudiant -->
<fieldset class="w3-section">
	<legend><b>Informations de l'étudiant</b></legend>
	<div class="w3-row-padding">

		<!-- Formation -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Votre formation :</label>
			<select
				id="title"
				name="id_Formation"
				class="w3-input w3-border w3-border-theme w3-theme-l4">

				<?php  foreach( Formation::selectAll( $dbh ) as $f ) : ?>

				<option
					value="<?php echo $f->id ?>"
					<?php if( $f->id == $u->id_formation ) echo "selected=\"selected\"" ?>>
					<?php echo $f->nom ?>
				</option>

				<?php endforeach; // Formation::selectAll() ?>
			</select>
		</div>

		<!-- dna -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Votre date de naissance :</label>

			<input
				type="date"
				name="dna"
				value="<?php echo $u->dna ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>







		</div>
		<!-- Préférences -->
		<div class="w3-col l12 m12 s12 w3-section">
			<label>Vos préférences :</label>
			<textarea
				name="preferences"
				rows="8"
				cols="80"
				class="w3-input w3-border w3-border-theme w3-theme-l4"><?php echo $u->preferences ?></textarea>
		</div>

	</div>
</fieldset>

<?php elseif( $u instanceof Entreprise ) : ?>

	<!-- Informations de l'entreprise -->
	<fieldset class="w3-section">
		<legend><b>Informations de l'entreprise</b></legend>
		<div class="w3-row-padding">

			<!-- Nom de l'entreprise -->
			<div class="w3-col l6 m12 s12 w3-section">
				<label>Nom :</label>
				<input
					type="text"
					name="nom_entreprise"
					value="<?php echo $u->nom_entreprise ?>"
					required="required"
					class="w3-input w3-border w3-border-theme w3-theme-l4"/>
			</div>

			<!-- Site Web -->
			<div class="w3-col l6 m12 s12 w3-section">
				<label>Site Web :</label>
				<input
					type="text"
					name="site_web"
					value="<?php echo $u->site_web ?>"
					required="required"
					class="w3-input w3-border w3-border-theme w3-theme-l4"/>
			</div>

			<!-- Description -->
			<div class="w3-col l12 m12 s12 w3-section">
				<label>Description :</label>
				<textarea
					name="desc_entreprise"
					rows="8"
					cols="80"
					class="w3-input w3-border w3-border-theme w3-theme-l4"><?php echo $u->desc_entreprise ?></textarea>
			</div>
		</div>
	</fieldset>

<?php endif; // $u ?>

<!-- Informations de contact -->
<fieldset class="w3-section">
	<legend><b>Informations de contact</b></legend>
	<div class="w3-row-padding">

		<!-- N° Voie -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>N° Voie :</label>
			<input
				type="text"
				name="no_voie"
				value="<?php echo $u->no_voie ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- Voie -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Voie :</label>
			<input
				type="text"
				name="voie"
				value="<?php echo $u->voie ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- Ville -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>Ville :</label>
			<input
				type="hidden"
				id="ville-id"
				name="id_Ville"
				value="<?php echo $u->id_ville ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
			<input
				type="text"
				id="ville-nom"
				value=""
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>

		<!-- N° Tel -->
		<div class="w3-col l6 m12 s12 w3-section">
			<label>N° de téléphone :</label>
			<input
				type="text"
				name="tel"
				value="<?php echo $u->tel ?>"
				required="required"
				class="w3-input w3-border w3-border-theme w3-theme-l4"/>
		</div>
	</div>
</fieldset>

<?php // BUG: La checkbox ne reste pas chécké u_u ) ?>

<label for="termes">
	<input
		id="termes"
		type="checkbox"
		name="termes"
		required="required"
		<?php if( $u->termes ) : ?>
		checked="checked"
		<?php endif; ?>/>
	J'accepte les <a href="#">conditions d'utilisation</a>.
</label>

<script type="text/javascript">
	$( document ).ready( function() {

		<?php if( $u->id_ville ) : ?>

		/* NOTE:
		 * On ne peut pas faire ca sur la page d'inscription car
		 * id_ville est null.
		 */

		// --- Récuperation du nom de la ville à partir de l'id. ---
		$.ajax({
			type     : 'GET',
			url      : './ville-query.php',
			dataType : 'json',
			data     : { id : <?php echo $u->id_ville ?> },
			timeout  : 3000,

			// --- success ---
			success  : function( data ) {
				$( '#ville-nom' ).val( data.nom + ' (' + data.cp + ')' );
			}
		});

		<?php endif; // $u->id_ville ?>

		// --- Autocomplète input ville-nom. ---
		$( '#ville-nom' ).autocomplete({

			// --- source ---
			source : function( request, response ) {
				$.ajax({
					type     : 'GET',
					url      : './ville-query.php',
					dataType : 'json',
					data		 : { like : request.term },
					timeout  : 3000,

					// --- success ---
					success : function( data ) {
						response( $.map( data, function( item ) {
							return {
								id		: item.id,
								value : item.nom + ' (' + item.cp + ')',
							}
						}))
					}
				});
			},
			minLength : 1,

			// --- select ---
			select : function( event, ui ) {
				$( '#ville-id' ).val( ui.item.id );
			}
		});
	});
</script>
