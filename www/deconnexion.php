<?php
	session_start();
	unset( $_SESSION['utilisateur'] );
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta http-equiv="refresh" content="3;url=./">
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="res\favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once dirname(__FILE__) . '/inc/header.inc.php'; ?>

		<!-- Main -->
		<main class="w3-theme-l5">
			<div class="w3-container w3-padding-64">
				<h1>Vous êtes déconnécté.</h1>
				<p><a href="./">&larr; Retour à l'accueil</a> (Redirection dans 3 secondes).</p>
				<p></p>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once dirname(__FILE__) . '/inc/footer.inc.php'; ?>
	</body>
</html>
