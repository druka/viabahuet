
<?php
	session_start();

	// On vérifie qu'un utilisateur est connécté.
	if( !isset( $_SESSION['utilisateur'] ) )
		header( 'Location: ./' );

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ville.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/offre.inc.php';

	// Edition d'une offre.
	if( isset( $_GET['id'] ) ) {
		$o = Offre::selectById(
			$dbh,
			$_GET['id']
		);

		// L'offre n'existe pas.
		if( !$o ) die( 'Cette offre n\'existe pas.' );

		$e = Entreprise::selectById(
			$dbh,
			$o->id_entreprise
		);
	}

	// Nouvelle offre.
	elseif( isset( $_GET['id_entreprise'] ) ) {
		$o = New Offre();

		$e = Entreprise::selectById(
			$dbh,
			$_GET['id_entreprise']
		);
	}
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once( './inc/header.inc.php' ); ?>

		<!-- Main -->
		<main class="w3-theme-l4">

			<!-- Layout -->
			<div class="w3-container w3-row">

				<!-- Sidebar -->
				<?php require_once( './inc/sidebar-entreprise.inc.php' ); ?>

				<!-- Colonne principale -->
				<div class="w3-rest w3-mobile">
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<form class="" action="./offre-proc.php" method="post">
							<h1>Edition d'une offre</h1>

							<!-- Général -->
							<fieldset class="w3-section">
								<legend><b>Général</b></legend>
								<div class="w3-row-padding">

									<!-- ID Offre -->
									<input
										type="hidden"
										name="id"
										value="<?php echo $o->id ?>"
										required="required" />

									<!-- ID Entreprise -->
									<input
										type="hidden"
										name="id_entreprise"
										value="<?php echo $e->id ?>"
										required="required" />

									<!-- Titre -->
									<div class="w3-col l12 m12 s12 w3-section">
										<label>Titre :</label>
										<input
											type="text"
											name="titre"
											value="<?php echo $o->titre ?>"
											required="required"
											class="w3-input w3-border w3-border-theme w3-theme-l4"/>
									</div>

									<!-- Description -->
									<div class="w3-col l12 m12 s12 w3-section">
										<label>Description :</label>
										<textarea
											name="description"
											rows="8"
											cols="80"
											class="w3-input w3-border w3-border-theme w3-theme-l4"><?php echo $o->description ?></textarea>
									</div>

								</div>
							</fieldset>

							<!-- Dates -->
							<fieldset class="w3-section">
								<legend><b>Dates</b></legend>
								<div class="w3-row-padding">

									<!-- Début -->
									<div class="w3-col l6 m12 s12 w3-section">
										<label>Date de début :</label>
										<input
											type="date"
											name="debut"
											value="<?php echo $o->debut ?>"
											required="required"
											class="w3-input w3-border w3-border-theme w3-theme-l4"/>
									</div>

									<!-- Fin -->
									<div class="w3-col l6 m12 s12 w3-section">
										<label>Date de fin :</label>
										<input
											type="date"
											name="fin"
											value="<?php echo $o->fin ?>"
											class="w3-input w3-border w3-border-theme w3-theme-l4"/>
									</div>

								</div>
							</fieldset>

							<!-- Rémunération -->
							<fieldset class="w3-section">
								<legend><b>Rémunération</b></legend>
								<div class="w3-row-padding">

									<!-- Salaire -->
									<div class="w3-col l6 m12 s12 w3-section">
										<label>Salaire horraire (HT) (€) :</label>
										<input
											type="number"
											name="salaire_hr_ht"
											value="<?php echo $o->salaire_hr_ht ?>"
											class="w3-input w3-border w3-border-theme w3-theme-l4"/>
									</div>

								</div>
							</fieldset>

							<?php
								if( isset( $_GET['id'] ) )
									$submit = 'Modifier';
								elseif( isset( $_GET['id_entreprise'] ) )
									$submit = 'Ajouter';
							?>

							<input
								class="w3-button w3-theme w3-margin-tom w3-margin-bottom"
								type="submit"
								name="submit"
								value="<?php echo $submit ?>"/>
						</form>
					</div>
				</div>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once( './inc/footer.inc.php' ); ?>
	</body>
</html>
