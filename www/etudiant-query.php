<?php
	session_start();

	// On vérifie qu'un utilisateur est connécté.
	if( !isset( $_SESSION['utilisateur'] ) )
		exit();

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ami.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ville.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/formation.inc.php';

	// L'utilisateur connécté.
	$u = unserialize( $_SESSION['utilisateur'] );

	if( isset( $_POST['search'] ) && $_POST['search'] ) {
		// Recherche initiée.
		$eleves = Eleve::like( $dbh, $_POST['q'] );
	} else {
		// Pas de recherche : on récupère tout !
		$eleves = Eleve::selectAll( $dbh );
	}
?>

<ul id="etudiants" class="w3-ul w3-margin-top w3-margin-bottom">

	<?php
		// Si la recherche à donnée quelque-chose :
		if( count( $eleves ) != 0 ) :
		// Parcours des élèves;
		foreach( $eleves as $e ) :
	?>

	<li class="w3-bar">

		<!-- Image de profile -->
		<div class="w3-container w3-cell w3-center w3-mobile">
			<div
				class="vb-profilepic vb-tiny w3-border"
				style="background-image: url( './images/<?php echo $e->photo ?>' )">
			</div><br/>
			<?php if( $u->id == $e->id ) : ?>
			<span class="w3-tag w3-theme">Vous</span>
			<?php endif // $u->id == $e->id ?>
		</div>

		<!-- Informations -->
		<div class="w3-container w3-cell w3-mobile">
			<!-- Prénom, nom -->
			<span class="w3-xlarge"><?php echo $e->prenom, ' ', $e->nom ?></span><br/>

			<?php
				// La ville ou est situé l'étudiant.
				$v = Ville::selectById( $dbh, $e->id_ville );
				// La formation de l'étudiant.
				$f = Formation::selectById( $dbh, $e->id_formation );
			?>

			<!-- Formation -->
			<span class="w3-margin-right w3-text-gray w3-mobile">
				<i class="fa fa-graduation-cap"></i>
				<em><?php echo $f->nom ?></em>
			</span>

			<!-- Localisation -->
			<span class="w3-margin-right w3-text-gray w3-mobile">
				<i class="fa fa-map-marker"></i>
				<em><?php echo $v->cp, ' ', $v->nom ?></em>
			</span>

			<!-- Préférences -->
			<p><?php echo nl2br( $e->preferences ) ?></p>

			<!-- Boutton Consulter -->
			<a
				href="./etudiant-view.php?id=<?php echo $e->id ?>"
				class="w3-button w3-text-theme w3-border w3-margin-bottom w3-mobile">
				<i class="fa fa-eye"></i>
				Consulter
			</a>

			<?php
				// On vérifie que c'est pas nous même.
				if( $e->id != $u->id ) :

				// Récuperations des amis.
				$a = Ami::SelectById( $dbh, $u->id, $e->id );

				// Cet utilisateur est-il un ami ?
				if( $a ) :
			?>

			<!-- Boutton Supprimer un ami -->
			<a
				id="ami-suppr-<?php echo $e->id ?>"
				onclick="queryAmi( <?php echo $e->id ?>, 'Supprimer' )"
				class="w3-button w3-text-gray w3-border w3-margin-bottom w3-mobile">
				<span class="label w3-text-res">
					<i class="fa fa-minus"></i>
					Retirer des amis
				</span>
			</a>

		<?php
			// Sinon, on ne veut pas s'ajouter soit-meme en ami.
			elseif( $e->id != $u->id ) :
		?>

			<!-- Boutton Ajouter en ami -->
			<a
				id="ami-ajout-<?php echo $e->id ?>"
				onclick="queryAmi( <?php echo $e->id ?>, 'Ajouter' )"
				class="w3-button w3-border w3-margin-bottom w3-mobile">
				<span class="label w3-text-theme">
					<i class="fa fa-plus"></i>
					Ajouter en ami
				</span>
			</a>

		<?php
			endif; // $a
			endif; // $e->id != $u->id
		?>

		</div>
	</li>

	<?php
		endforeach; // $e
	?>

</ul>

<?php
	// Aucun étidiant n'a été trouvé.
	else :
?>

<p class="w3-text-gray"><em>Aucun étudiant correspondant n'a été trouvée.</em></p>

<?php endif; // count( $eleves ) ?>
