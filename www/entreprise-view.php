<?php
	session_start();

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/ville.inc.php';

	// Récupèration de l'entreprise/
	$e = Entreprise::selectById( $dbh, $_GET['id'] );
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
		<!-- JQuery -->
		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
		<!-- Script offre -->
		<script
			type="text/javascript"
			src="./js/offre.js"></script>

		<script>
			// Quand la page est ready :
			$( document ).ready( function() {
				// Quand quelque chose est rentrée dans la textbox :
				$( '#search' ).keyup( function() {

					var q = $( '#search' ).val();
					var search = 1;

					// Requête AJAX
					$.ajax({
						url : './offre-query.php',
						method : 'POST',
						data : {
							search : search,
							q : q,
							id_entreprise : <?php echo $e->id ?>,
						},
						success : function( data ) {
							$( '#resultats' ).html( data );
						},
						dataType : 'text'
					});
				});

				$( '#search' ).keyup();
			});

			// Fonction d'affichage du modal de suppression d'une offre.
			function showOffreSuppr( id ) {
				$( '#offre-suppr-modal' ).load(
					'./inc/offre-suppr-modal.inc.php?id=' + id
				);

				$( '#offre-suppr-modal' ).show();
			}

			// Quand le bouton 'Oui' du modal de suppression d'une offre est cliqué.
			function offreSupprOui( id ) {
				// Suppression de l'offre.
				queryOffre( id, 'Supprimer' );

				$( '#offre-suppr-modal' ).hide();

				// Màj après suppression.
				$( '#search' ).keyup();
			}
		</script>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once( './inc/header.inc.php' ); ?>

		<!-- Main -->
		<main class="w3-theme-l4">

			<!-- Layout -->
			<div class="w3-container w3-row">

				<!-- Sidebar -->
				<?php require_once( './inc/sidebar-entreprise.inc.php' ); ?>

				<!-- Colonne principale -->
				<div class="w3-rest w3-mobile">

					<!-- Description -->
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<h1><?php echo $e->nom_entreprise ?></h1>
						<p><?php echo nl2br( $e->desc_entreprise ) ?></p>
					</div>

					<!-- Offres -->
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<div class="w3-cell-row">
							<div class="w3-cell w3-cell-middle w3-mobile">
								<h1>Offres</h1>
							</div>

							<!-- Recherche -->
							<div class="w3-cell w3-cell-middle w3-mobile">
								<input
									type="text"
									placeholder="Recherche"
									id="search"
									class="w3-right w3-border w3-white w3-margin-top w3-margin-bottom w3-mobile"/>
							</div>
						</div>
						<hr/>

						<!-- Resultats -->
						<div id="resultats">
							<p>Chargement ...</p>
						</div>
					</div>

				</div>
			</div>

			<!-- Modal de suppression d'une offre. -->
			<div id="offre-suppr-modal" class="w3-modal"></div>
		</main>

		<!-- Pied -->
		<?php require_once( './inc/footer.inc.php' ); ?>
	</body>
</html>
