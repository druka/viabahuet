<?php
	session_start();

	if( !isset( $_SESSION['utilisateur'] ) ) {
		header( 'Location: ./' );
		exit();
	}

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/formation.inc.php';
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- JQuery CSS -->
		<link
			rel="stylesheet"
			href="/resources/demos/style.css">
		<!-- JQueryUI CSS -->
		<link
			rel="stylesheet"
			href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
		<!-- JQuery -->
		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
		<!-- JQueryUI -->
		<script
			src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once( './inc/header.inc.php' ); ?>

		<!-- Main -->
		<main class="w3-theme-l4">

			<!-- Layout -->
			<div class="w3-container w3-row">

				<!-- Sidebar -->
				<?php require_once( './inc/sidebar-profile.inc.php' ); ?>

				<!-- Colonne principale -->
				<div class="w3-rest w3-mobile">
					<div class="w3-container w3-card w3-round w3-margin w3-white">
						<h1>Edition du profile</h1>
						<form
							action="./profile-proc.php"
							method="POST"
							enctype="multipart/form-data">
							<?php require_once dirname(__FILE__) . '/inc/profile-form.inc.php' ?>
							<p>
								<input
									class="w3-button w3-theme"
									type="submit"
									name="submit"
									value="Modifier"
									required="required">
								<!-- <input
									class="w3-button w3-red"
									type="submit"
									name="submit"
									value="Supprimer"
									required="required"> -->
							</p>
						</form>
					</div>
				</div>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once( './inc/footer.inc.php' ); ?>
	</body>
</html>
