<?php
	session_start();

	require_once dirname(__FILE__) . '/inc/bdd.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/utilisateur.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';

	// Recherche d'un élève correspondant.
	$eleve = Eleve::selectByLogin(
		$dbh,
		$_POST['mail'],
		$_POST['mdp' ]
	);

	// Recherche d'une entreprise correspondante.
	$entreprise = Entreprise::selectByLogin(
		$dbh,
		$_POST['mail'],
		$_POST['mdp' ]
	);

	if( isset( $eleve ) )
		$utilisateur = $eleve;
	elseif( isset( $entreprise ) )
		$utilisateur = $entreprise;
	else
		$utilisateur = null;

	if( isset( $utilisateur ) ) {
		// Un utilisateur à été trouvé.
		$_SESSION['utilisateur'] = serialize( $utilisateur );
		header( 'Location: ./ ');
	}

	else {
		echo "<p>Aucun utilisateur trouvé <a href=\"./\">(&larr;)</a>.</p>";
	}

?>
