<?php
	session_start();

	if( isset( $_SESSION['utilisateur'] ) ) {
		header( 'Location: ./offre.php' );
		exit();
	}
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- JQueryUI CSS -->
		<link
			rel="stylesheet"
			href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
		<!-- JQuery -->
		<script
			src="https://code.jquery.com/jquery-3.3.1.min.js"
			integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			crossorigin="anonymous"></script>
		<!-- JQueryUI -->
		<script
			src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once dirname(__FILE__) . '/inc/header.inc.php'; ?>

		<!-- Main -->
		<main class="w3-theme-l5">
			<div class="w3-container w3-padding-64">
				<h1>Inscription</h1>
				<form
					action="./profile-proc.php"
					method="POST"
					enctype="multipart/form-data">
					<fieldset class="w3-section">
						<legend><b>Vous êtes :</b></legend>
						<div class="w3-center w3-text-large">
							<label
								for="type-étudiant"
								class="w3-button w3-theme-l4 w3-round-xxlarge w3-border w3-border-theme w3-section w3-mobile">
								<input
									id="type-étudiant"
									type="radio"
									name="type"
									value="Un étudiant"
									required="required"
									onclick="$('#form').load( './inc/inscription-form.inc.php?u=etudiant' );"/>
								Un étudiant
							</label>
							<label
								for="type-entreprise"
								class="w3-button w3-theme-l4 w3-round-xxlarge w3-border w3-border-theme w3-section w3-mobile">
								<input
									id="type-entreprise"
									type="radio"
									name="type"
									value="Une entreprise"
									required="required"
									onclick="$('#form').load( './inc/inscription-form.inc.php?u=entreprise' );"/>
								Une entreprise
							</label>
						</div>
					</fieldset>
					<div id="form"></div>
					<p>
						<input
							class="w3-button w3-theme"
							type="submit"
							name="submit"
							value="Inscription"
							required="required">
					</p>
				</form>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once dirname(__FILE__) . '/inc/footer.inc.php'; ?>
	</body>
</html>
