<?php
	require_once dirname( __FILE__ ) . '/inc/bdd.inc.php';
	require_once dirname( __FILE__ ) . '/inc/classes/ville.inc.php';

	if( isset( $_GET['id'] ) )
		$res = Ville::selectById( $dbh, $_GET['id'] );
	elseif( isset( $_GET['like'] ) )
		$res = Ville::like( $dbh, $_GET['like'] );

	echo json_encode( $res );
?>
