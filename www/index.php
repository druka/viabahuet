<?php
	session_start();

	require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
	require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';

	if( isset( $_SESSION['utilisateur'] ) ) {
		// L'utilisateur connécté.
		$u = unserialize( $_SESSION['utilisateur'] );

		if( $u instanceof Eleve )
			header( 'Location: ./etudiant-view.php?id=' . $u->id );
		if( $u instanceof Entreprise )
			header( 'Location: ./entreprise-view.php?id=' . $u->id );

		exit();
	}
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>ViaBahuet</title>
		<!-- Favicon -->
		<link
			rel="icon"
			type="image/ico"
			href="./res/favicon.ico"/>
		<!-- W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/w3css/4/w3.css"/>
		<!-- Theme W3.CSS -->
		<link
			rel="stylesheet"
			href="https://www.w3schools.com/lib/w3-theme-indigo.css"/>
		<!-- Font Awesome -->
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<!-- Fonte Roboto -->
		<link
			rel="stylesheet"
			href="https://fonts.googleapis.com/css?family=Roboto"/>
		<!-- Master CSS -->
		<link
			rel="stylesheet"
			href="./css/master.css"/>
	</head>
	<body class="w3-theme-d5">

		<!-- En-tête -->
		<?php require_once dirname(__FILE__) . '/inc/header.inc.php'; ?>

		<!-- Main -->
		<main class="w3-theme-l5">

			<!-- Formulaire de connexion -->
			<div class="w3-theme-l3">
				<div class="w3-container">
					<form class="w3-right" action="./connexion.php" method="post">
						<input
							class="w3-input w3-section w3-mobile w3-border w3-border-theme w3-light-grey"
							type="text"
							name="mail"
							placeholder="Adresse e-mail"
							style="display: inline-block; width: 10em;"/>
						<input
							class="w3-input w3-section w3-mobile w3-border w3-border-theme w3-light-grey"
							type="password"
							name="mdp"
							placeholder="Mot de passe"
							style="display: inline-block; width: 10em;"/>
						<input
							class="w3-input w3-section w3-mobile w3-border w3-border-theme w3-theme-d5"
							type="submit"
							name="submit"
							value="&rarr;"
							style="display: inline-block; width: auto;"/>
					</form>
				</div>
			</div>

			<div class="w3-container w3-padding-64">
				<h1>Bienvenue</h1>
				<p>ViaBahuet est le réseau social du lycée Bahuet.</p>
				<p><img src="./res/logo.png" alt=""></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<p><a href="./inscription.php">Inscription &rarr;</a></p>
			</div>
		</main>

		<!-- Pied -->
		<?php require_once dirname(__FILE__) . '/inc/footer.inc.php'; ?>
	</body>
</html>
