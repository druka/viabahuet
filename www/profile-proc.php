<?php
session_start();
require_once dirname(__FILE__) . '/inc/bdd.inc.php';
require_once dirname(__FILE__) . '/inc/utils.inc.php';
require_once dirname(__FILE__) . '/inc/classes/eleve.inc.php';
require_once dirname(__FILE__) . '/inc/classes/entreprise.inc.php';

if( !isset( $_POST['submit'] ) ) {
	header( 'Location: ./' );
	exit();
}

/* NOTE: si checkbox checkée, $_POST['termes'] = 'ON'
et nous on veut true.*/
if( isset( $_POST["termes"] ) )
	$_POST['termes'] = 1;
else
	$_POST['termes'] = 0;


switch( $_POST['submit'] ) {
	// --- Inscription ---
	case 'Inscription':
		switch( $_POST['type'] ) {
			case 'Un étudiant':
				$nu = New Eleve( $_POST );

				// $_POST["termes"] = "on"
				break;
			case 'Une entreprise':
				$nu = New Entreprise( $_POST );
				break;
		}

		// Upload photo.
		if( $_FILES['photo_up']['size'] != 0 )
			$nu->photo = uploadImage( $_FILES['photo_up'] );

		if( $nu instanceof Eleve )
			Eleve::Insert( $dbh, $nu );
		if( $nu instanceof Entreprise )
			Entreprise::Insert( $dbh, $nu );

		break; // 'Inscription'

	// --- Modification ---
	case 'Modifier':
		if( !isset( $_SESSION['utilisateur'] ) )
			throw New Exception( "Vous n'êtes pas connécté !" );

		if( $_POST['mail'] != $_POST['mail-check'] )
			throw New Exception(
				"Les adresses E-mail ne correspondent pas."
			);

		if( $_POST['mdp'] != $_POST['mdp-check'] )
			throw New Exception(
				"Les Mots de passes ne correspondent pas."
			);

		$u = unserialize( $_SESSION['utilisateur'] );

		if( $u instanceof Eleve )
			$nu = New Eleve( $_POST );
		if( $u instanceof Entreprise )
			$nu = New Entreprise( $_POST );

		// Supressions de la photo ?
		if( isset( $_POST['photo_suppr'] ) ) {
			$nu->photo = null;
			if( $u->photo ) unlink( "./images/" . $u->photo );
		}

		// Upload photo.
		if( $_FILES['photo_up']['size'] != 0 ) {
			$nu->photo = uploadImage( $_FILES['photo_up'] );
			if( $u->photo ) unlink( "./images/" . $u->photo );
		}

		if( $nu instanceof Eleve )
			Eleve::update( $dbh, $u->id, $nu );
		if( $nu instanceof Entreprise )
			Entreprise::update( $dbh, $u->id, $nu );

		// On applique les changements.
		$nu->id = $u->id;
		$_SESSION['utilisateur'] = serialize( $nu );

		break; // 'Modifier'
}

//echo '<pre>'; var_dump( $_POST ); echo '</pre>';
header( 'Location: ./' );

?>
